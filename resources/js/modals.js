$(() => {
    const $confirmationButton = $(".confirmation-button");
    const $confirmationModal = $("#confirmation-modal");
    const $confirmationModalLabel = $("#confirmation-modal-label");
    const $confirmationModalBody = $("#confirmation-modal-body");

    $confirmationButton.on("click", function (event) {
        const label = $(this).data("label");
        const body = $(this).data("body");
        const href = $(this).attr("href");

        event.preventDefault();

        $confirmationModalLabel.html(label);
        $confirmationModalBody.html(body);
        $confirmationModal.find(".confirm").on("click", function () {
            window.location = href;
        });

        $confirmationModal.modal("show");
    });
});
