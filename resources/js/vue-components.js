import { createApp } from "vue/index.js";
import { camelCase, upperFirst } from "lodash";

// We create an empty Vue app to append our components to
const app = createApp({});

// We load all files from our "components" directory
const requireComponent = require.context("./components", true, /.\.(vue|js)$/);

// We automatically register all components
requireComponent.keys().forEach((fileName) => {
    // We get the component config
    const componentConfig = requireComponent(fileName);

    // We get the pascal-cased name of the component
    const componentName = upperFirst(
        camelCase(
            // We get the file name regardless of directory depth
            fileName.split("/")
                .pop()
                .replace(/\.\w+$/, "")
        )
    );

    // We register the component globally
    app.component(componentName, componentConfig.default || componentConfig);
});

// We mount the app to the parent content div
// so that the components may be loaded anywhere
app.mount("#app-content");
