$(() => {
    ($("body")).tooltip({
        // option required for tooltips not to flicker
        // inside the .table-responsive div element
        boundary: "window",

        selector: '[data-bs-toggle="tooltip"]',
        animation: false,

        // option required for tooltips not to trigger
        // on buttons clicks
        trigger: "hover",
    });
});
