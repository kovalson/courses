function dropdownSelectHandler(event) {
    const $this = $(this);
    const target = $this.data("target");
    const id = target.split("#input-")[1];
    const value = $this.data("value");
    const label = $this.data("label");

    event.preventDefault();

    $(target).val(value);
    $("[data-target='" + target + "']").removeClass("active");
    $this.addClass("active");
    $("#" + id).html(label);
}

$(() => {
    $(".bootstrap-dropdown-select").click(dropdownSelectHandler);
});
