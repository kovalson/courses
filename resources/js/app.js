require("./bootstrap.js");

$(() => {
    // We load the form validation snippet
    require("./form-validation");

    // Load and register all Vue components
    require("./vue-components.js");

    // The logout functionality
    require("./logout.js");

    // Initialize tooltips
    require("./tooltips.js");

    // Confirmation modal functionality
    require("./modals.js");

    require("./libs/tinymce");
    require("./libs/jquery.tinymce");

    // Functionality of dropdown lists that behave
    // just like select inputs
    require("./dropdown-select.js");
});
