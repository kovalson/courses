$(() => {
    const $logoutLink = $("#logout-link");
    const $logoutForm = $("#logout-form");

    // We submit the logout form when clicking the logout link
    $logoutLink.on("click", (event) => {
        event.preventDefault();

        $logoutForm.submit();
    });
});
