/**
 * Returns subset of given array that autocomplete given string.
 *
 * @param array
 * @param string
 * @param limit
 * @return Array
 */
function autocomplete(array, string, limit = 5) {
    const filteredString = normalizeText(string);
    let counter = 0;

    return array
        .filter((element) => normalizeText(element)
        .includes(filteredString) && (counter++ < limit));
}

/**
 * Replaces diacritics and other special characters in text
 * and converts it to lower case.
 *
 * @param {string} text
 * @return {string}
 */
function normalizeText(text) {
    return text
        .trim()
        .normalize("NFD")
        .replace(/[\u0300-\u036f]/g, "")
        .toLowerCase();
}

export default autocomplete;
