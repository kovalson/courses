@extends("layouts.app")

@section("content")
    @includeWhen(isset($courses), "course.index", ["courses" => $courses])
@endsection
