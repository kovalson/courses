@php
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
/** @var LengthAwarePaginator $courses */
/** @var LengthAwarePaginator $categories */
/** @var LengthAwarePaginator $tags */
@endphp

@extends("layouts.app")

@section("content")

    @include("components.page-header", ["title" => __("Wyniki wyszukiwania dla \":word\"", ["word" => request()->get("q")])])

    @if (isset($courses))
        <h4 class="text-muted">@lang("Znalezione szkolenia - :count", ["count" => $courses->total()])</h4>
        @include("partials.course.paginated", ["courses" => $courses])
    @endif

    @if (isset($categories))
        <h4 class="text-muted">@lang("Znalezione kategorie - :count", ["count" => $categories->total()])</h4>
        @include("partials.category.paginated", ["categories" => $categories])
    @endif

    @if (isset($tags))
        <h4 class="text-muted">@lang("Znalezione tagi - :count", ["count" => $tags->total()])</h4>
        @include("partials.tag.paginated", ["tags" => $tags])
    @endif

@endsection
