@extends("layouts.app")

@section("content")
    @include("components.page-header", ["title" => __("Wyszukiwanie zaawansowane")])

    <label for="tags-input">@lang("Wyszukaj po tagach")</label>
    <div class="course-tags">
        <input type="text" name="tags" id="tags-input" class="form-control tags-input w-auto">
    </div>
{{--    {!! Form::open(["route" => "search.advanced", "method" => "get"]) !!}--}}
{{--        {{ Form::label("tags", __("Szukaj po tagach")) }}--}}
{{--        {{ Form::text("tags", null, ["id" => "tags"]) }}--}}
{{--    {!! Form::close() !!}--}}
@endsection
