@extends("layouts.app")

@section("content")
    @include("components.page-header", ["title" => __("Zaloguj się"), "icon" => "fas fa-key"])

    {!! Form::open(["route" => "login"]) !!}
        <div class="row">
            <div class="col-md-8 col-lg-6 col-xl-5">
                {{ Form::bsEmail("email", null, ["required" => true, "floating" => true]) }}
                {{ Form::bsPassword("password", null, ["required" => true, "floating" => true]) }}
                {{ Form::bsCheckbox("remember", null, ["label" => __("Zapamiętaj mnie")]) }}
                {{ Form::bsSubmit(__("Zaloguj się")) }}
            </div>
        </div>
    {!! Form::close() !!}
@endsection
