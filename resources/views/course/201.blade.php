@extends("layouts.app")

@section("content")
    @include("components.page-header", ["title" => __("Szkolenie utworzone")])
    <p class="lead">@lang("Dziękujemy za zgłoszenie szkolenia!")</p>
    <p>@lang("Twoje szkolenie zostało prawidłowo utworzone i przekazane do akceptacji przez administratora. Po zaakceptowaniu pojawi się ono w wyszukiwarce, odpowiedniej kategorii oraz w zakładce najnowszych szkoleń.")</p>
@endsection
