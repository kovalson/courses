@include("components.page-header", ["title" => __("Szkolenia")])

@include("partials.course.paginated", ["courses" => $courses])
