@php
use App\Models\Course;
/** @var Course $course */
@endphp

@extends("layouts.app")

@section("content")
    @include("components.content-navigation")

    @component("components.window")
        <header>
            <div class="row">
                <div class="col-md-3">
                    <div class="text-center mb-3">
                        <img src="{{ get_course_image_url($course) }}" class="img-thumbnail"
                             alt="" aria-hidden="true">
                    </div>
                    <div>
                        @if ($course->hasCategory())
                            <div>
                                <div class="fw-bold">@lang("Dla")</div>
                                <div>
                                    <a href="{{ route("category.courses", ["slug" => $course->category->slug]) }}">{{ $course->category->name }}</a>
                                </div>
                            </div>
                        @endif
                        <div class="mt-3">
                            <div class="fw-bold">@lang("Termin rozpoczęcia")</div>
                            <div>{{ $course->getFromDate() }}</div>
                        </div>
                        <div class="mt-3">
                            <div class="fw-bold">@lang("Termin zakończenia")</div>
                            <div>{{ $course->getToDate() }}</div>
                        </div>
                        <div class="mt-3">
                            <div class="fw-bold">@lang("Miejsce")</div>
                            <div>{{ $course->place }}</div>
                        </div>
                        <div class="mt-3">
                            <div class="fw-bold">@lang("Zgłoszenia do")</div>
                            <div>{{ $course->getApplicationDeadlineDate() }}</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <h1>{{ $course->title }}</h1>
                    @if ($course->created_at)
                        <p class="text-muted mb-0">@lang("Utworzono: :date", ["date" => pretty_date($course->created_at)])</p>
                    @endif
                    @if ($course->updated_at && $course->updated_at->notEqualTo($course->created_at))
                        <p class="text-muted mt-0">@lang("Ostatnia aktualizacja: :date", ["date" => pretty_date($course->updated_at)])</p>
                    @endif
                    @if (!empty($course->description))
                        <div class="text-break text-justify mt-4">
                            {!! $course->description !!}
                        </div>
                    @endif
                    @if ($course->hasTags())
                        <div class="course-tags">
                            @include("partials.course.tags", ["tags" => $course->tags])
                        </div>
                    @endif
                </div>
            </div>
        </header>
    @endcomponent
@endsection
