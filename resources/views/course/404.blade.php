@extends("layouts.app")

@section("content")
    @include("components.page-header", ["title" => __("Nie znaleziono szkolenia")])
    <p class="fs-5">@lang("Szkolenie, które próbujesz wyświetlić nie istnieje. Upewnij się, że szukasz w odpowiednim miejscu.")<br>@lang("Możliwe, że nazwa szkolenia uległa zmianie lub zostało ono ukryte. W takim przypadku możesz skorzystać z wyszukiwarki serwisu, aby znaleźć słowa kluczowe związane z żądanym szkoleniem.")</p>
@endsection
