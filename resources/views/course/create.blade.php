@php
use App\Models\Course;
use Illuminate\Support\Collection;
/** @var Course $course */
/** @var Collection $tags */

$course = isset($course) ? $course : null;
@endphp

@extends("layouts.app")

@section("content")
    @include("components.page-header", ["title" => $course ? __("Edytuj szkolenie") : __("Dodaj szkolenie")])

    <p>@lang("Wypełnij poniższy formularz, aby dodać nowe szkolenie. Utworzone szkolenie zostanie następnie przekazane do akceptacji przez administratora.")</p>

    {!! Form::open(["route" => $course ? ["course.update", "course" => $course->slug] : "course.store", "method" => "post", "class" => "needs-validation", "novalidate" => true]) !!}

        {{ Form::bsText("title", $course ? $course->title : old("title"), [
            "label" => __("Nazwa szkolenia"),
            "help" => __("<b>Pole wymagane</b>, maksymalnie 255 znaków"),
            "validation" => __("Nazwa szkolenia jest nieprawidłowa"),
            "attributes" => ["required"],
            "floating",
        ]) }}

        <div class="row">
            <div class="col-md-4">
                {{ Form::bsSelect("category_id", $categoriesOptions, $course ? $course->category_id : old("category_id"), [
                    "label" => __("Wybierz kategorię"),
                    "help" => __("Dla kogo przeznaczone jest szkolenie"),
                    "validation" => __("Należy wybrać kategorię szkolenia"),
                    "attributes" => ["required"],
                    "floating",
                ]) }}
            </div>
            <div class="col-md-8 mt-3 mt-md-0">
                {{ Form::bsText("image_url", $course ? $course->image_url : old("image_url"), [
                    "label" => __("Adres URL grafiki szkolenia"),
                    "help" => __("Opcjonalnie możesz podać adres URL do oficjalnej grafiki szkolenia"),
                    "floating",
                ]) }}
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-6">
                {{ Form::bsText("from", $course ? input_date($course->from) : old("from"), [
                    "label" => __("Data i czas rozpoczęcia"),
                    "help" => __("Data i czas, na przykład 01.01.2020 10:00"),
                    "validation" => __("Nieprawidłowa data i czas rozpoczęcia"),
                    "attributes" => [
                        "pattern" => "[0-9]{2}\.[0-9]{2}\.[0-9]{4}\s[0-9]{2}:[0-9]{2}",
                        "required",
                    ],
                    "floating",
                ]) }}
            </div>
            <div class="col-md-6 mt-3 mt-md-0">
                {{ Form::bsText("to", $course ? input_date($course->to) : old("to"), [
                    "label" => __("Data i czas zakończenia"),
                    "help" => __("Data i czas, na przykład 03.01.2020 12:30"),
                    "validation" => __("Nieprawidłowa data i czas zakończenia"),
                    "attributes" => [
                        "pattern" => "[0-9]{2}\.[0-9]{2}\.[0-9]{4}\s[0-9]{2}:[0-9]{2}",
                        "required",
                    ],
                    "floating",
                ]) }}
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-6">
                {{ Form::bsText("application_deadline", $course ? input_date($course->application_deadline) : old("application_deadline"), [
                    "label" => __("Ostatni dzień przyjmowania zgłoszeń"),
                    "help" => __("Data i czas, na przykład 01.09.2021 23:59"),
                    "validation" => __("Nieprawidłowa data i czas"),
                    "attributes" => [
                        "pattern" => "[0-9]{2}\.[0-9]{2}\.[0-9]{4}\s[0-9]{2}:[0-9]{2}",
                        "required",
                    ],
                    "floating",
                ]) }}
            </div>
        </div>

        {{ Form::bsText("place", $course ? $course->place : old("place"), [
            "label" => __("Miejsce szkolenia"),
            "help" => __("Podaj dokładny adres lub szczegółowo opisz miejsce szkolenia"),
            "validation" => __("Należy podać miejsce szkolenia"),
            "attributes" => ["required"],
            "floating",
        ]) }}

        {{ Form::textarea("description", $course ? $course->description : old("description"), ["class" => "form-control"]) }}

        {{ Form::bsTags("tags", $tags, $course ? $course->getTagsNames() : collect([])) }}

        @auth
            {{ Form::bsCheckbox("published", $course ? $course->published : false, [
                "label" => __("Opublikowane"),
                "help" => __("Zaznacz, jeśli szkolenie ma zostać automatycznie opublikowane"),
            ]) }}

            {{ Form::bsCheckbox("archived", $course ? $course->archived : false, [
                "label" => __("Archiwizowane"),
                "help" => __("Zaznacz, jeśli szkolenie ma zostać automatycznie zarchiwizowane"),
            ]) }}
        @endauth

        {{ Form::bsCaptcha("captcha", [
            "validation" => __("Należy zaznaczyć checkbox"),
        ]) }}

        {{ Form::submit($course ? __("Zapisz dane szkolenia") : __("Dodaj szkolenie i przekaż do akceptacji"), [
            "class" => "btn btn-primary btn-lg mt-4",
            "id" => "submit-button",
        ]) }}

    {!! Form::close() !!}
@endsection

@once
    @push("scripts")
        <script type="application/javascript">
            $(() => {
                $("textarea").tinymce({
                    placeholder: "@lang("Opis szkolenia wraz ze wszystkimi niezbędnymi informacjami dotyczącymi organizacji")",
                    skin: "bootstrap",
                    toolbar: "undo redo bold italic alignleft aligncenter alignright bullist numlist",
                    language: "pl",
                });
            });
        </script>
    @endpush
@endonce
