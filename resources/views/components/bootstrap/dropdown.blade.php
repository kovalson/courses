@php
$value = isset($value) ? $value : null;
$listClass = isset($listClass) ? $listClass : "";
@endphp

{{ $dropdownToggle }}
<ul class="dropdown-menu {{ $listClass }}" aria-labelledby="{{ $id }}">
    @foreach ($options as $option => $label)
        <li>
            <a class="dropdown-item bootstrap-dropdown-select @if ($option === $value) active @endif" href="#"
               data-target="#input-{{ $id }}" data-value="{{ $option }}" data-label="{{ $label }}">{{ $label }}</a>
        </li>
    @endforeach
</ul>
<input type="hidden" name="{{ $name }}" value="{{ $value }}" id="input-{{ $id }}">
