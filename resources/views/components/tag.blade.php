@php
$color = isset($color) ? "text-" . $color : "";
$background = isset($background) ? "bg-" . $background : "";
@endphp

<div class="badge {{ $color }} {{ $background }}">
    {{ $slot }}
</div>
