@php
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
/** @var LengthAwarePaginator $pagination */
@endphp

@if ($pagination->total() > 0)
    <nav class="d-md-flex align-items-center my-3" aria-label="@lang("Stronicowanie")">
        @include("components.pagination.aside", ["pagination" => $pagination, "class" => "d-block d-md-none text-center mb-3"])
        {{ $pagination->onEachSide(1)->appends(request()->query())->links() }}
        @include("components.pagination.aside", ["pagination" => $pagination, "class" => "d-none d-md-flex ms-auto"])
    </nav>
@endif
