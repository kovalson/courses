@php
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
/** @var LengthAwarePaginator $pagination */
/** @var string $class */
@endphp

<aside class="{{ $class }}">
    @lang("Wyświetlanie :from - :to z :total", [
        "from" => $pagination->firstItem(),
        "to" => $pagination->lastItem(),
        "total" => $pagination->total(),
    ])
</aside>
