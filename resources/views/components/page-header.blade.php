@php
$hasIcon = isset($icon);
$icon = isset($icon) ? $icon : "";
$filters = isset($filters) ? $filters : [];
@endphp


<header class="d-flex align-items-center mb-3 text-black-50">
    <div class="h2 mb-0 me-3 w-100">@includeWhen($hasIcon, "partials.icon", ["icon" => $icon, "class" => "me-2"]){{ $title }}</div>
    @includeWhen(!empty($filters), "components.filters.filters", ["filters" => $filters])
</header>
