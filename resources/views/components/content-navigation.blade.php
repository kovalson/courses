@if (url()->current() !== url()->previous())
    <div class="mb-3">
        <a href="{{ url()->previous() }}">Powrót</a>
    </div>
@endif
