<div class="window bg-white p-4 border rounded shadow-sm">
    {{ $slot }}
</div>
