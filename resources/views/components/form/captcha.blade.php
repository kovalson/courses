<checkbox-captcha name="{{ $name }}" label="@lang("Zaznacz, jeśli jesteś człowiekiem")">
    @include("components.form.validation", ["params" => $params, "name" => $name])
</checkbox-captcha>
