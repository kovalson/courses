@php
use Illuminate\Contracts\Support\MessageBag;

/**
 * @var string $name
 * @var mixed $value
 * @var array $params
 * @var array $defaultParams
 * @var MessageBag $errors
 */

// Default parameters
$type = Arr::get($defaultParams, "type", "text");
$name = $name ?? Arr::get($defaultParams, "name");
$label = Arr::get($defaultParams, "label");

// Parameters
$label = Arr::get($params, "label") ?? $label;

// Input attributes
$inputAttributes = Arr::get($params, "attributes", []);
Arr::set($inputAttributes, "type", $type);
Arr::set($inputAttributes, "id", Arr::get($inputAttributes, "id", $name));
Arr::set($inputAttributes, "class", trim("form-control " . Arr::get($inputAttributes, "class", "")));
Arr::set($inputAttributes, "placeholder", Arr::get($inputAttributes, "placeholder", $label));
if ($errors->has($name)) {
    Arr::set($inputAttributes, "class", Arr::get($inputAttributes, "class") . " is-invalid");
}

// Label attributes
$labelAttributes = Arr::get($params, "labelAttributes", []);
Arr::set($labelAttributes, "class", trim("form-label " . Arr::get($labelAttributes, "class", "")));

// Components
$labelComponent = Form::label($name, $label, $labelAttributes);
$inputComponent = Form::input($type, $name, $value ?? old($name), $inputAttributes);

// Optionals
$formFloating = in_array("floating", $params);
@endphp

<div class="form-group @if ($formFloating) form-floating @endif">
    @if (!$formFloating)
        {{ $labelComponent }}
    @endif

    {{ $inputComponent }}

    @if ($formFloating)
        {{ $labelComponent }}
    @endif

    @include("components.form.help", ["params" => $params])

    @include("components.form.validation", ["params" => $params, "name" => $name])
</div>
