@php
/** @var array $params */

$help = Arr::get($params, "help", "");
@endphp

@if ($help)
    <div class="form-text">{!! $help !!}</div>
@endif
