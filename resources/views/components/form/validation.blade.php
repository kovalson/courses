@php
/**
 * @var array $params
 * @var string $name
 */

$validation = Arr::get($params, "validation", "");
@endphp

@if ($validation)
    <div class="small invalid-feedback">{!! $validation !!}</div>
@elseif ($errors->has($name))
    <div class="small invalid-feedback">{{ $errors->first($name) }}</div>
@endif
