@php
/** @var array $name */
/** @var array $value */
/** @var array $params */

$label = Arr::get($params, "label");
$checked = $value ? "checked" : "";
@endphp

<div class="form-group">
    <input type="checkbox" class="form-check-input" id="{{ $name }}" name="{{ $name }}" @if ($checked) checked @endif>
    <label for="{{ $name }}" class="form-check-label ms-2">{{ $label }}</label>
    @include("components.form.help", ["params" => $params])
</div>
