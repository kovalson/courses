@php
use Illuminate\Support\Collection;
/** @var string $name */
/** @var Collection $tags */
/** @var Collection $selected */

$tagInputName = $name . "-input";
@endphp

<div class="form-group">
    <div id="tags-input-{{ $name }}">
        <tags-input
            id="tags-input"
            name="{{ $name }}"
            label="@lang("Wpisz tagi")"
            remove-label="@lang("Usuń tag")"
            :tags="{{ json_encode($tags->values(), JSON_UNESCAPED_UNICODE) }}"
            :selected-tags="{{ json_encode($selected->values(), JSON_UNESCAPED_UNICODE) }}"
        ></tags-input>
    </div>
</div>
