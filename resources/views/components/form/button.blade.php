@php
/** @var array $params */

$type = Arr::get($params, "type", "submit");
$class = Arr::get($params, "class", "");
$id = Arr::get($params, "id", "");
$disabled = (Arr::has($params, "disabled") && Arr::get($params, "disabled") === true);
@endphp

<button
    type="{{ $type }}"
    class="btn btn-primary {{ $class }}"
    @if ($id) id="{{ $id }}" @endif
    @if ($disabled) disabled @endif
>
    {{ $value }}
</button>
