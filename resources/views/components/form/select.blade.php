@php
/**
 * @var string $name
 * @var array $options
 * @var mixed $selected
 * @var array $params
 */

// Params
$label = Arr::get($params, "label", $name);

// Select attributes
$selectAttributes = Arr::get($params, "attributes", []);
Arr::set($selectAttributes, "class", trim("form-select " . Arr::get($selectAttributes, "class", "")));

// Label attributes
$labelAttributes = Arr::get($params, "labelAttributes", []);
Arr::set($labelAttributes, "class", trim("form-label " . Arr::get($labelAttributes, "class", "")));

// Components
$selectComponent = Form::select($name, $options, $selected, $selectAttributes);
$labelComponent = Form::label($name, $label, $labelAttributes);

// Optionals
$formFloating = in_array("floating", $params);
@endphp

<div class="form-group @if ($formFloating) form-floating @endif">
    @if (!$formFloating)
        {{ $labelComponent }}
    @endif

    {{ $selectComponent }}

    @if ($formFloating)
        {{ $labelComponent }}
    @endif

    @include("components.form.help", ["params" => $params])

    @include("components.form.validation", ["params" => $params, "name" => $name])
</div>
