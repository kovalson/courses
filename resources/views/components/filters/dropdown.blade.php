@php
use App\Services\Filters\Filter;
/** @var Filter $filter */

$filterKey = $filter->getKey();
$filterIcon = $filter->getIcon();
$filterLabel = $filter->getLabel();
$filterValue = $filter->getValue();
$defaultOption = $filter->getDefaultOption();
$availableOptions = $filter->getAvailableOptions();
$filterValueLabel = $filter->isSet() ? $availableOptions[$filterValue] : "";
@endphp

<div class="btn-group" role="group" data-bs-toggle="tooltip" title="{{ $filterLabel }}">
    <button type="button" class="btn btn-white dropdown-toggle no-caret" id="dropdown-{{ $filterKey }}"
            data-bs-toggle="dropdown" aria-expanded="false"
            aria-label="{{ $filterLabel }}">
        <i class="{{ $filterIcon }} fa-fw @if ($filter->isSet()) me-1 @endif"></i><span class="d-none d-md-inline-block">{{ $filterValueLabel }}</span>
    </button>
    <ul class="dropdown-menu dropdown-menu-md-end" aria-labelledby="dropdown-{{ $filterKey }}">
        @foreach ($availableOptions as $option => $label)
            <li>
                <a class="dropdown-item @if ($filterValue === $option) active @endif"
                   href="{{ set_filter_url($filterKey, $option === $defaultOption ? null : $option) }}">{{ $label }}</a>
            </li>
        @endforeach
    </ul>
</div>
