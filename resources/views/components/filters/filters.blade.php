@php
use App\Services\Filters\Filter;
/** @var array $filters */
/** @var Filter $filter */
@endphp

<div class="btn-group justify-content-end" role="group" aria-label="@lang("Filtry")">
    @foreach ($filters as $filter)
        @includeWhen($filter->isApplicable(), "components.filters." . $filter->type, ["filter" => $filter])
    @endforeach
</div>
