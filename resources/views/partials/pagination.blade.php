@php
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
/** @var LengthAwarePaginator $paginator */
@endphp

@if ($paginator->hasPages())
    <nav>
        <ul class="pagination">
            {{-- First Page Link --}}
            <li class="page-item @if ($paginator->onFirstPage()) disabled @endif">
                <a href="{{ $paginator->url(1) }}" class="page-link" title="@lang("Pierwsza")">&laquo;</a>
            </li>

            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang("Poprzednia")">
                    <span class="page-link" aria-hidden="true">&lsaquo;</span>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev"
                       title="@lang("Poprzednia")" aria-label="@lang("Poprzednia")">&lsaquo;</a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-item disabled" aria-disabled="true">
                        <span class="page-link">{{ $element }}</span>
                    </li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active" aria-current="page">
                                <span class="page-link">{{ $page }}</span>
                            </li>
                        @else
                            <li class="page-item">
                                <a class="page-link" href="{{ $url }}"
                                   title="@lang("Strona :page", ["page" => $page])">{{ $page }}</a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next"
                       title="@lang("Następna")" aria-label="@lang("Następna")">&rsaquo;</a>
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang("Następna")">
                    <span class="page-link" aria-hidden="true">&rsaquo;</span>
                </li>
            @endif

            {{-- Last Page Link --}}
            <li class="page-item @if ($paginator->currentPage() === $paginator->lastPage()) disabled @endif">
                <a href="{{ $paginator->url($paginator->lastPage()) }}" class="page-link"
                   title="@lang("Ostatnia")">&raquo;</a>
            </li>
        </ul>
    </nav>
@endif
