{!! Form::open(["route" => "search", "method" => "get", "class" => "d-flex ms-auto"]) !!}
    <div class="input-group">
        <input class="form-control" type="search" name="q" placeholder="@lang("Szukaj")..."
               aria-label="@lang("Szukaj")">
        <button class="btn btn-primary" type="submit" aria-label="@lang("Szukaj")">
            <i class="fas fa-search"></i>
        </button>
    </div>
{!! Form::close() !!}
