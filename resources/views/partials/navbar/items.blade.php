<ul class="navbar-nav d-flex ms-auto">
    @auth
        <li class="nav-item">
            <a href="{{ route("categories.index") }}" class="nav-link">@lang("Kategorie")</a>
        </li>
        <li class="nav-item">
            {!! Form::open(["route" => "logout", "id" => "logout-form"]) !!}
            <a href="{{ route("logout") }}" class="nav-link" id="logout-link">@lang("Wyloguj")</a>
            {!! Form::close() !!}
        </li>
    @endauth
    @guest
        <li class="nav-item">
            <a href="{{ route("login") }}" class="nav-link">@lang("Zaloguj się")</a>
        </li>
    @endguest
</ul>
