@php
use App\Services\SidebarMenu\SidebarMenuItem;
/** @var SidebarMenuItem[] $items */
/** @var int $level */

$level = isset($level) ? $level : 1;
@endphp

<ul class="nav flex-column">
    @foreach ($items as $item)
        @if ($item->hasItems())
            <li class="nav-item">
                <div class="small text-uppercase text-black px-4 pt-4 pb-2 fw-bold sidebar-header"
                     title="{{ $item->name }}">
                    <a href="{{ $item->url }}">{{ $item->name }}</a>
                </div>
                @include("partials.sidebar-menu", ["items" => $item->items, "level" => 1])
            </li>
        @else
            <li class="nav-item @if (request()->url() === $item->url) active @endif">
                <a class="nav-link px-4" href="{{ $item->url }}" title="{{ $item->name }}">{{ $item->name }}</a>
            </li>
        @endif
        @if ($item->hasMore())
            <li class="nav-item mt-1 mx-4">
                <a href="{{ $item->urlCategories }}" class="small text-muted"
                   title="@lang("Więcej w kategorii: :category", ["category" => $item->name])"
                   aria-label="@lang("Więcej w kategorii: :category", ["category" => $item->name])">@lang("Więcej...")</a>
            </li>
        @endif
    @endforeach
</ul>
