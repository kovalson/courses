@php
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
/** @var LengthAwarePaginator $categories */
@endphp

@include("components.pagination.pagination", ["pagination" => $categories])

@if ($categories->total() > 0)
    @foreach ($categories as $item)
        @include("partials.category.category", ["category" => $item])
    @endforeach
@else
    <p class="lead">@lang("Nie znaleziono kategorii.")</p>
@endif

@include("components.pagination.pagination", ["pagination" => $categories])
