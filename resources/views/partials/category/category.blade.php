@php
use App\Models\Category;
use App\Models\Course;
/** @var Category $category */
/** @var Course $course */
@endphp

<div class="card category-card shadow-sm">
    <div class="card-header bg-white rounded py-3">
        <h5 class="my-0">
            <a href="{{ route("category.courses", $category->slug) }}" class="card-title">{{ $category->name }}</a>
        </h5>
    </div>
    <div class="card-body">
        <span>@lang("Szkolenia w kategorii - :count", ["count" => $category->courses()->count()])</span>
    </div>
    @auth
        <div class="card-footer">
            <ul class="list-inline mb-0">
                <li class="list-inline-item">
                    <a href="{{ route("categories.edit", ["slug" => $category->slug]) }}">@lang("Edytuj")</a>
                </li>
                @if (!$category->isFirst())
                    <li class="list-inline-item">&bull;</li>
                    <li class="list-inline-item">
                        <a href="{{ route("categories.move.up", ["slug" => $category->slug]) }}">@lang("Przesuń do góry")</a>
                    </li>
                @endif
                @if (!$category->isLast())
                    <li class="list-inline-item">&bull;</li>
                    <li class="list-inline-item">
                        <a href="{{ route("categories.move.down", ["slug" => $category->slug]) }}">@lang("Przesuń w dół")</a>
                    </li>
                @endif
                <li class="list-inline-item">&bull;</li>
                <li class="list-inline-item">
                    <a href="{{ route("categories.destroy", ["slug" => $category->slug]) }}"
                       class="confirmation-button" data-label="@lang("Usuń kategorię")"
                       data-body="@lang("Czy na pewno chcesz usunąć tę kategorię?")">@lang("Usuń")</a>
                </li>
            </ul>
        </div>
    @endauth
</div>
