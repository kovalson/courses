@php
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
/** @var LengthAwarePaginator $tags */
@endphp

@include("components.pagination.pagination", ["pagination" => $tags])

@if ($tags->total() > 0)
    @foreach ($tags as $item)
        @include("partials.tag.tag", ["tag" => $item])
    @endforeach
@else
    <p class="lead">@lang("Nie znaleziono tagów.")</p>
@endif

@include("components.pagination.pagination", ["pagination" => $tags])
