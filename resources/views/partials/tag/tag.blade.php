@php
use App\Models\Tag;
/** @var Tag $tag */
@endphp

<div class="card category-card shadow-sm">
    <div class="card-header bg-white rounded py-3">
        <h5 class="my-0">
            <a href="{{ route("tag", $tag->slug) }}" class="card-title">{{ $tag->name }}</a>
        </h5>
    </div>
    <div class="card-body">
        @lang("Szkoleń z tagiem - :count", ["count" => $tag->courses()->count()])
    </div>
</div>
