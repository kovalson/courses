<div class="alert alert-{{ $type }} app-message-{{ $type }} alert-dismissible fade show shadow-sm mb-4" role="alert">
    <span class="icon me-2">
        <i class="{{ get_alert_icon($type) }} fa-fw"></i>
    </span>
    <span>{{ $slot }}</span>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
