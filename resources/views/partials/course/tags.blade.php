<div class="course-tags mt-4">
    @foreach ($tags as $tag)<a href="{{ route("tag", $tag->slug) }}" class="badge fw-normal text-muted border">{{ $tag->name }}</a>@endforeach
</div>
