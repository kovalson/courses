@php
use App\Models\Course;
/** @var Course $course */
@endphp

<div class="card course-card shadow-sm">
    <div class="row g-0">
        <div class="col-md-3 border-end">
            <img src="{{ get_course_image_url($course) }}" class="course-thumbnail img-thumbnail border-0" alt=""
                 aria-hidden="true">
        </div>
        <div class="col-md-9">
            <div class="card-body">
                @includeWhen(auth()->check(), "partials.course.auth.details", ["course" => $course])
                <div class="h4 d-flex align-items-center mt-0 mb-1">
                    <a href="{{ route("course", $course->slug) }}" class="card-title mb-0">{{ $course->title }}</a>
                </div>
                @includeWhen($course->category()->exists(), "partials.course.category", ["category" => $course->category])
                <ul class="list-unstyled mt-4">
                    <li>@lang("Termin rozpoczęcia"): {{ $course->getFromDate() }}</li>
                    <li>@lang("Termin zakończenia"): {{ $course->getToDate() }}</li>
                    <li>@lang("Miejsce"): {{ $course->place }}</li>
                    <li>@lang("Ostatni dzień przyjmowania zgłoszeń to") <strong>{{ $course->getApplicationDeadlineDate() }}</strong></li>
                </ul>
                @include("partials.course.tags", ["tags" => $course->tags])
            </div>
        </div>
        @auth
            <div class="card-footer">
                <ul class="list-inline mb-0">
                    <li class="list-inline-item">
                        <a href="{{ route("course.edit", ["course" => $course->slug]) }}">@lang("Edytuj")</a>
                    </li>
                    <li class="list-inline-item">&bull;</li>
                    <li class="list-inline-item">
                        @if ($course->published)
                            <a href="{{ route("course.hide", ["course" => $course->slug]) }}"
                               class="confirmation-button" data-label="@lang("Ukryj szkolenie")"
                               data-body="@lang("Czy na pewno chcesz ukryć szkolenie?")">@lang("Ukryj")</a>
                        @else
                            <a href="{{ route("course.publish", ["course" => $course->slug]) }}"
                               class="confirmation-button" data-label="@lang("Opublikuj szkolenie")"
                               data-body="@lang("Czy na pewno chcesz opublikować szkolenie?")">@lang("Opublikuj")</a>
                        @endif
                    </li>
                    <li class="list-inline-item">&bull;</li>
                    <li class="list-inline-item">
                        @if ($course->archived)
                            <a href="{{ route("course.restore", ["course" => $course->slug]) }}"
                               class="confirmation-button" data-label="@lang("Przywróć z archiwum")"
                               data-body="@lang("Czy na pewno chcesz przywrócić szkolenie z archiwum?")">@lang("Przywróć z archiwum")</a>
                        @else
                            <a href="{{ route("course.archive", ["course" => $course->slug]) }}"
                               class="confirmation-button" data-label="@lang("Archiwizuj szkolenie")"
                               data-body="@lang("Czy na pewno chcesz archiwizować szkolenie?")">@lang("Archiwizuj")</a>
                        @endif
                    </li>
                    <li class="list-inline-item">&bull;</li>
                    <li class="list-inline-item">
                        <a href="{{ route("course.destroy", ["course" => $course->slug]) }}"
                           class="confirmation-button" data-label="@lang("Usuń szkolenie")"
                           data-body="@lang("Czy na pewno chcesz usunąć szkolenie?")">@lang("Usuń")</a>
                    </li>
                </ul>
            </div>
        @endauth
    </div>
</div>
