@php
use App\Models\Course;
/** @var Course $course */
@endphp

<div class="card-subtitle course-tags small mb-2">
    @if ($course->archived)
        <small class="badge border text-muted">
            <i class="fas fa-archive fa-fw me-1"></i>
            @lang("Archiwum")
        </small>
    @endif
    @if (!$course->published)
        <small class="badge border text-muted">
            <i class="far fa-eye-slash fa-fw me-1"></i>
            @lang("Nieopublikowane")
        </small>
    @endif
</div>
