<small class="card-subtitle">
    @lang("Dla") <a href="{{ route("category.courses", $category->slug) }}">{{ mb_lcfirst($category->name) }}</a>
</small>
