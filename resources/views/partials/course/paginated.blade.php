@php
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
/** @var LengthAwarePaginator $courses */
@endphp

@include("components.pagination.pagination", ["pagination" => $courses])

@if ($courses->total() > 0)
    @foreach ($courses as $item)
        @include("partials.course.course", ["course" => $item])
    @endforeach
@else
    <p class="lead">@lang("Nie znaleziono szkoleń.")</p>
@endif

@include("components.pagination.pagination", ["pagination" => $courses])
