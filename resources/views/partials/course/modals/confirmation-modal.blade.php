<div class="modal fade" id="confirmation-modal" tabindex="-1" aria-labelledby="confirmation-modal-label"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmation-modal-label">@lang("Potwierdzenie")</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="confirmation-modal-body">
                @lang("Potwierdź akcję")
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white dismiss" data-bs-dismiss="modal">@lang("Zamknij")</button>
                <button type="button" class="btn btn-primary confirm">@lang("Potwierdź")</button>
            </div>
        </div>
    </div>
</div>
