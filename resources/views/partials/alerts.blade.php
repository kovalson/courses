@php
use Illuminate\Support\MessageBag;
/** @var MessageBag $errors */
@endphp

@if ($errors->any())
    @component("partials.alert", ["type" => "danger"])
        @if ($errors->count() > 1)
            @lang("Wystąpiły następujące błędy:")
            <ul class="mb-0 mt-2">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @else
            {{ $errors->first() }}
        @endif
    @endcomponent
@endif

@if (session("success"))
    @component("partials.alert", ["type" => "success"])
        {{ session("success") }}
    @endcomponent
@endif
