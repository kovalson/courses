@if (isset($icon))
    <i class="{{ $icon }} {{ isset($class) ? $class : "" }}"></i>
@endif
