@php
use App\Models\Tag;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
/** @var Tag $tag */
/** @var LengthAwarePaginator $courses */
@endphp

@extends("layouts.app")

@section("content")
    @include("components.page-header", ["title" => __("Tag - :tag", ["tag" => $tag->name])])

    @include("partials.course.paginated", ["courses" => $courses])
@endsection
