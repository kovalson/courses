@extends("layouts.app")

@section("content")
    @include("components.page-header", ["title" => __("Nie znaleziono tagu")])
    <p class="fs-5">@lang("Tag, po którym próbujesz szukać nie istnieje. Upewnij się, że szukasz w dobrym miejscu")<br>@lang("Możliwe, że nazwa tagu uległa zmianie. W takim przypadku możesz skorzystać z wyszukiwarki serwisu, aby znaleźć podobne tagi.")</p>
@endsection
