@php
use App\Models\Category;
/**
 * @var array $categories
 * @var Category $category
 */
@endphp

@extends("layouts.app")

@section("content")
    @include("components.page-header", ["title" => __("Kategorie")])

    <div class="mb-3">
        <a href="{{ route("categories.create") }}">Dodaj kategorię</a>
    </div>
    @foreach ($categories as $category)
        @include("partials.category.category", ["category" => $category])

        @if ($category->hasSubcategories())
            <div class="ms-2 ps-4 mb-3 border-start">
                @foreach ($category->subcategories as $subcategory)
                    @include("partials.category.category", ["category" => $subcategory])
                @endforeach
            </div>
        @endif
    @endforeach
@endsection
