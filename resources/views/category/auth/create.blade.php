@php
use App\Models\Category;
use Illuminate\Support\Collection;
/** @var Collection $baseCategories */
/** @var Category $category */

$category = isset($category) ? $category : null;
@endphp

@extends("layouts.app")

@section("content")

    @include("components.page-header", ["title" => $category ? __("Edytuj kategorię") : __("Dodaj kategorię")])

    {!! Form::open(["route" => $category ? ["categories.update", "slug" => $category->slug] : "categories.store", "method" => "post", "class" => "needs-validation", "novalidate" => true]) !!}

        {{ Form::bsText("name", $category ? $category->name : old("name"), [
            "label" => __("Nazwa kategorii"),
            "help" => __("<b>Pole wymagane</b>, maksymalnie 255 znaków"),
            "validation" => __("Nazwa szkolenia jest nieprawidłowa"),
            "attributes" => ["required"],
            "floating",
        ]) }}

        {{ Form::bsSelect("category_id", $baseCategories, $category ? $category->category_id : null, [
            "label" => __("Wybierz kategorię"),
            "help" => __("Dla kogo przeznaczone jest szkolenie"),
            "validation" => __("Należy wybrać kategorię szkolenia"),
            "floating",
        ]) }}

        {{ Form::submit($category ? __("Zapisz kategorię") : __("Dodaj kategorię"), [
            "class" => "btn btn-primary btn-lg",
            "id" => "submit-button",
        ]) }}

    {!! Form::close() !!}

@endsection
