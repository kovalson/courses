@php
use App\Models\Category;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
/** @var Category $category */
/** @var LengthAwarePaginator $categories */
@endphp

@extends("layouts.app")

@section("content")
    @include("components.page-header", ["title" => $category->name])
    @include("partials.category.paginated", ["categories" => $categories])
@endsection
