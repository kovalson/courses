@extends("layouts.app")

@section("content")
    @include("components.page-header", ["title" => __("Nie znaleziono kategorii")])
    <p class="fs-5">@lang("Kategoria, którą próbujesz wyświetlić nie istnieje. Upewnij się, że szukasz w odpowiednim miejscu.")<br>@lang("Możliwe, że nazwa kategorii uległa zmianie. W takim przypadku możesz skorzystać z wyszukiwarki serwisu, aby znaleźć słowa kluczowe związane z żądaną kategorią.")</p>
@endsection
