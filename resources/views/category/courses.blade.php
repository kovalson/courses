@php
use App\Models\Category;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
/** @var Category $category */
/** @var LengthAwarePaginator $courses */
/** @var array $filters */
@endphp

@extends("layouts.app")

@section("content")
    @include("components.page-header", ["title" => __("Kategoria - :category", ["category" => $category->name])])
    @include("partials.course.paginated", ["courses" => $courses])
@endsection
