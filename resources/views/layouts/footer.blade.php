<footer class="footer border-top d-flex align-items-center">
    <div class="container d-flex align-items-center">
        <div>
            <a href="http://lubuska.zhp.pl" target="_blank">
                <img src="{{ asset("img/logo-circle.png") }}" alt="{{ __("Chorągiew Ziemi Lubuskiej") }}" height="42">
            </a>
        </div>
        <div class="ms-3">
            <a href="https://fanimani.pl" target="_blank">
                <img src="{{ asset("img/logo-fanimani-small.png") }}" alt="{{ __("Fani Mani") }}" height="42">
            </a>
        </div>
        <small class="ms-auto">v{{ Config::get("APP_VERSION") }}<span class="mx-2 text-dark">&bull;</span>@lang("Copyright") &copy; {{ date("Y") }}</small>
    </div>
</footer>
