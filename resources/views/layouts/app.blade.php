<!doctype html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="Krzysztof Tatarynowicz">
    <meta name="description"
          content="Internetowy bank form kształceniowych organizowanych na poziomie chorągwi">
    <meta name="keywords" content="ZHP, Chorągiew Ziemi Lubuskiej, Chorągiew Ziemi Lubuskiej ZHP, Chorągiew Ziemi Lubuskiej Związku Harcerstwa Polskiego, Chorągiew, Internetowy Bank Pomysłów, pomysły, kształcenie, formy kształceniowe">

    {{-- The page title --}}
    <title>{{ config("app.name") }}</title>

    {{-- Scripts --}}
    <script src="{{ asset("js/app.js") }}"></script>
    @stack("scripts")

    {{-- Styles --}}
    <link rel="stylesheet" href="{{ asset("css/app.css") }}" type="text/css">
    @stack("styles")
</head>
<body>
    @include("partials.course.modals.confirmation-modal")
    <div class="app-content" id="app-content">
        @include("layouts.header")
        <div class="container my-3">
            <div class="row">
                <div class="col-12">
                    @include("layouts.search")
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-lg-3 col-xl-3">
                    @include("layouts.sidebar")
                </div>
                <div class="col-md-9 col-lg-9 col-xl-9">
                    @include("partials.alerts")
                    @yield("content")
                </div>
            </div>
        </div>
        <div class="footer-push"></div>
    </div>
    @include("layouts.footer")
</body>
</html>
