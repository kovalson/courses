<nav class="navbar navbar-expand-lg navbar-dark">
    <div class="container">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-content"
                aria-controls="navbar-content" aria-expanded="false" aria-label="@lang("Przełącz nawigację")">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-content">
            <ul class="navbar-nav d-flex">
                <li class="nav-item">
                    <a href="{{ route("home") }}" class="nav-link">@lang("Szkolenia")</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route("course.create") }}" class="nav-link">@lang("Dodaj szkolenie")</a>
                </li>
            </ul>
            @include("partials.navbar.items")
        </div>
    </div>
</nav>
