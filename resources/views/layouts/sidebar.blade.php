@inject("menu", "App\Services\SidebarMenu\SidebarMenuService")

<button class="d-block d-md-none btn btn-outline-primary mb-3" type="button"
        data-bs-toggle="collapse" data-bs-target="#sidebar" aria-expanded="false"
        aria-controls="navbar"><i class="fas fa-bars fa-fw me-2"></i>@lang("Menu")</button>

<nav class="sidebar bg-white border rounded shadow-sm mb-3 mb-md-0 pb-4" id="sidebar">
    @if ($menu->hasItems())
        @include("partials.sidebar-menu", ["items" => $menu->items])
    @else
        <p>@lang("Nie ustawiono żadnych linków. Dodaj nowe kategorie, aby stworzyć menu.")</p>
    @endif
</nav>
