<header class="pb-5 app-header">
    @include("layouts.navbar")
    <div class="container text-white text-center text-md-start mt-4">
        <h1 class="display-4 fw-bolder">@lang("Internetowy Bank Szkoleń")</h1>
        <p class="lead">@lang("Przeglądaj i buduj szeroką paletę różnych form kształceniowych organizowanych na poziomie chorągwianym.")</p>
    </div>
</header>
