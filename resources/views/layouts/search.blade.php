@inject("searchFilter", "App\Services\Filters\NavbarSearchFilter")

<div class="mb-3 text-center text-md-end">
    {!! Form::open(["route" => "search", "method" => "get", "class" => "mb-2", "role" => "search"]) !!}
        <div class="input-group">
            @component("components.bootstrap.dropdown", [
                "id" => "searchFilterDropdown",
                "name" => $searchFilter->getKey(),
                "options" => $searchFilter->getAvailableOptions(),
                "value" => $searchFilter->getValue(),
                "listClass" => "fs-5",
            ])
                @slot("dropdownToggle")
                    <button type="button" class="btn btn-lg btn-white dropdown-toggle" id="searchFilterDropdown"
                            data-bs-toggle="dropdown" aria-expanded="false">{{ $searchFilter->getValueLabel() }}</button>
                @endslot
            @endcomponent
            <input type="text" class="form-control form-control-lg" id="searchInput"
                   name="q" value="{{ request()->get("q") }}" aria-label="@lang("Szukaj w Banku Szkoleń")"
                   role="searchbox" placeholder="@lang("Szukaj w Banku Szkoleń...")">
        </div>
    {!! Form::close() !!}
</div>
