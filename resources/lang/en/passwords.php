<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    "reset" => "Twoje hasło zostało zaktualizowane!",
    "sent" => "Na podany adres e-mail został wysłany link do formularza resetującego hasło.",
    "throttled" => "Musisz poczekać przed kolejną próbą.",
    "token" => "Token resetujący hasło jest nieprawidłowy. Spróbuj ponownie.",
    "user" => "Nie znaleziono użytkownika z podanym adresem e-mail.",

];
