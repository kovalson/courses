<?php

namespace {
    exit("This file should not be included, only analyzed be the IDE.");
}

namespace Illuminate\Contracts\Auth {

    use App\Models\User;

    interface Guard
    {
        /**
         * Returns currently authenticated user.
         *
         * @return User
         */
        public function user(): User;
    }
}

namespace Collective\Html {

    use Illuminate\Support\Collection;

    class FormFacade
    {
        /**
         * Returns custom Bootstrap submit button.
         *
         * @param string $value
         * @param array $params
         * @return string
         */
        public static function bsSubmit(string $value, array $params = []): string
        {
            /** @var FormFacade $instance */
            return $instance->bsSubmit($value, $params);
        }

        /**
         * Returns custom Bootstrap text input.
         *
         * @param string $name
         * @param null $value
         * @param array $params
         * @return string
         */
        public static function bsText(string $name, $value = null, array $params = []): string
        {
            /** @var FormFacade $instance */
            return $instance::bsText($name, $value, $params);
        }

        /**
         * Returns custom Bootstrap email input.
         *
         * @param string $name
         * @param null $value
         * @param array $params
         * @return string
         */
        public static function bsEmail(string $name, $value = null, array $params = []): string
        {
            /** @var FormFacade $instance */
            return $instance::bsEmail($name, $value, $params);
        }

        /**
         * Returns custom Bootstrap password input.
         *
         * @param string $name
         * @param null $value
         * @param array $params
         * @return string
         */
        public static function bsPassword(string $name, $value = null, array $params = []): string
        {
            /** @var FormFacade $instance */
            return $instance::bsEmail($name, $value, $params);
        }

        /**
         * Returns custom Bootstrap select list.
         *
         * @param string $name
         * @param array $options
         * @param null $selected
         * @param array $params
         * @return string
         */
        public static function bsSelect(string $name, array $options = [], $selected = null, array $params = []): string
        {
            /** @var FormFacade $instance */
            return $instance::bsSelect($name, $options, $selected, $params);
        }

        /**
         * Returns custom Bootstrap checkbox input.
         *
         * @param string $name
         * @param null $value
         * @param array $params
         * @return string
         */
        public static function bsCheckbox(string $name, $value = null, array $params = []): string
        {
            /** @var FormFacade $instance */
            return $instance::bsCheckbox($name, $value, $params);
        }

        /**
         * Returns custom Bootstrap captcha component.
         *
         * @param string $name
         * @param array $params
         * @return string
         */
        public static function bsCaptcha(string $name = "captcha", array $params = []): string
        {
            /** @var FormFacade $instance */
            return $instance::bsCaptcha($name, $params);
        }

        /**
         * Returns custom Bootstrap tags input.
         *
         * @param string $name
         * @param Collection|array $tags
         * @param Collection|array $selected
         * @param array $params
         * @return string
         */
        public static function bsTags(string $name, $tags = [], $selected = [], array $params = []): string
        {
            /** @var FormFacade $instance */
            return $instance::bsTags($name, $tags, $selected, $params);
        }
    }

}
