<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("courses", function (Blueprint $table) {
            $table->id();
            $table->foreignId("category_id")
                ->nullable()
                ->constrained()
                ->cascadeOnUpdate()
                ->nullOnDelete();
            $table->string("title")->nullable(false);
            $table->string("slug")->unique()->nullable(false);
            $table->string("image_url")->nullable();
            $table->dateTime("from")->nullable(false);
            $table->dateTime("to")->nullable(false);
            $table->dateTime("application_deadline")->nullable(false);
            $table->string("place")->nullable(false);
            $table->text("description")->nullable();
            $table->boolean("published")->default(false);
            $table->boolean("archived")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("courses", function (Blueprint $table) {
            $table->dropForeign(["category_id"]);
        });
        Schema::dropIfExists("courses");
    }
}
