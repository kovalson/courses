<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create("tags", function (Blueprint $table) {
            $table->increments("id");
            $table->string("name");
            $table->string("slug");
            $table->boolean("published")->nullable(false)->default(false);
            $table->timestamps();
        });

        Schema::create("taggables", function (Blueprint $table) {
            $table->integer("tag_id")->unsigned();
            $table->morphs("taggable");

            $table->unique(["tag_id", "taggable_id"]);

            $table->foreign("tag_id")
                ->references("id")->on("tags")
                ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop("taggables");
        Schema::drop("tags");
    }
}
