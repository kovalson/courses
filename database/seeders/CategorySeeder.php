<?php
declare(strict_types=1);
namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * The total number of base categories to create.
     *
     * @var int
     */
    const CATEGORIES_COUNT = 4;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $this->seedCategories();

        $this->seedSubcategories();

        $this->fixCategoriesPositions();
    }

    /**
     * Seeds the base categories.
     *
     * @return void
     */
    private function seedCategories(): void
    {
        Category::factory()
            ->times(self::CATEGORIES_COUNT)
            ->create();
    }

    /**
     * Seeds the subcategories for base categories.
     *
     * @return void
     */
    private function seedSubcategories(): void
    {
        for ($i = 1; $i <= self::CATEGORIES_COUNT; $i++) {
            Category::factory()
                ->times(rand(5, 9))
                ->state(["category_id" => $i])
                ->create();
        }
    }

    /**
     * Sets the "position" column value of each category to
     * be a incrementally valid.
     *
     * @return void
     */
    private function fixCategoriesPositions(): void
    {
        $counts = [
            null => 1,
        ];

        for ($i = 1; $i <= self::CATEGORIES_COUNT; $i++) {
            $counts[$i] = 1;
        }

        Category::all()->each(function (Category $category) use (&$counts) {
            $category->update([
                "position" => $counts[$category->category_id]++
            ]);
        });
    }
}
