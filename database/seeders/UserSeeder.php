<?php
declare(strict_types=1);
namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = $this->getDefaultUsersData();

        foreach ($users as $user) {
            User::getModel()->insertOrIgnore($user);
        }
    }

    /**
     * Returns array of default users to create.
     *
     * @return array[]
     */
    private function getDefaultUsersData(): array
    {
        return [
            [
                "name" => "Krzysztof Tatarynowicz",
                "email" => "ktatarynowicz@hotmail.com",
                "email_verified_at" => now(),
                "password" => '$2y$10$NWAWFhG2TIxNFcL8BlUROOHtDAigVFqQuXm3Bg4JJ7fKB1cRJAEkq',
                "active" => true,
            ],
            [
                "name" => "Edyta Walent",
                "email" => "edyta.walent@zhp.net.pl",
                "email_verified_at" => now(),
                "password" => '$2y$10$kBj8NKxLwCBc9169dwNdbuOnFvND.0ogiRH/HJlqNDVtgvyxD1fAa',
                "active" => true,
            ],
        ];
    }
}
