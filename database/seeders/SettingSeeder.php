<?php
declare(strict_types=1);
namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultSettings = $this->getDataProvider();

        foreach ($defaultSettings as $setting) {
            $attributes = Arr::only($setting, "key");

            $values = [
                "type" => $setting["type"],
                "key" => $setting["key"],
                "value" => $setting["value"],
            ];

            Setting::updateOrCreate($attributes, $values);
        }
    }

    /**
     * Returns default data provider for settings.
     *
     * @return array
     */
    private function getDataProvider(): array
    {
        return [
            [
                "type" => Setting::INT,
                "key" => Setting::MENU_ITEMS_LIMIT,
                "value" => 5,
            ],
            [
                "type" => Setting::INT,
                "key" => Setting::CATEGORIES_PER_PAGE,
                "value" => 10,
            ],
            [
                "type" => Setting::INT,
                "key" => Setting::COURSES_PER_PAGE,
                "value" => 10,
            ],
            [
                "type" => Setting::INT,
                "key" => Setting::TAGS_PER_PAGE,
                "value" => 10,
            ],
        ];
    }
}
