<?php
declare(strict_types=1);
namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * The min number of tags to create.
     *
     * @type int
     */
    const MIN_TAGS_COUNT = 20;

    /**
     * The max number of tags to create.
     *
     * @type int
     */
    const MAX_TAGS_COUNT = 40;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tagsCount = $this->getTagsCount();

        Tag::factory()
            ->times($tagsCount)
            ->create();
    }

    /**
     * Returns the number of tags to create.
     *
     * @return int
     */
    private function getTagsCount(): int
    {
        return mt_rand(self::MIN_TAGS_COUNT, self::MAX_TAGS_COUNT);
    }
}
