<?php
declare(strict_types=1);
namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $name = $this->faker
            ->unique()
            ->realText(rand(20, 40));

        $name = trim($name, ".");

        return [
            "name" => Str::ucfirst($name),
            "slug" => Str::slug($name),
            "position" => 0,
            "category_id" => null,
        ];
    }
}
