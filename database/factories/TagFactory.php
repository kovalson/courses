<?php
declare(strict_types=1);
namespace Database\Factories;

use App\Models\Tag;
use Illuminate\Database\Eloquent\Factories\Factory;

class TagFactory extends Factory
{
    /**
     * Factor that determines (arbitrarily) how many
     * characters are one word.
     *
     * @type int
     */
    const CHARACTERS_TO_WORDS_FACTOR = 10;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tag::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $wordsNumber = $this->getRandomWordsNumber();
        $words = $this->getNotEmptyText($wordsNumber);

        return [
            "name" => $words,
            "published" => true,
        ];
    }

    /**
     * Returns random number of words for tag name.
     *
     * @return int
     */
    private function getRandomWordsNumber(): int
    {
        $value = mt_rand(1, 100) > 75 ? 1 : 2;

        return $value * self::CHARACTERS_TO_WORDS_FACTOR;
    }

    /**
     * Returns not empty text.
     *
     * @param int $charactersNumber
     * @param bool $trimFullStops
     * @return string
     */
    private function getNotEmptyText(int $charactersNumber, bool $trimFullStops = true): string
    {
        $text = "";

        while (empty($text)) {
            $text = $this->faker->realText($charactersNumber);

            if ($trimFullStops) {
                $text = trim($text, ".");
            }

            if (!empty($text)) {
                break;
            }
        }

        return $text;
    }
}
