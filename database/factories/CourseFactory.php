<?php
declare(strict_types=1);
namespace Database\Factories;

use App\Models\Category;
use App\Models\Course;
use App\Models\Tag;
use DateInterval;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CourseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Course::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $duration = rand(0, 3);
        $years = rand(0, 1);
        $title = $this->faker
            ->unique()
            ->realText(rand(45, 70));
        $title = trim($title, ".");
        $from = $this->faker->dateTimeThisYear->add(new DateInterval("P${years}Y"));
        $to = (clone $from)->add(new DateInterval("P${duration}D"));
        $applicationDeadline = (clone $from)->sub(new DateInterval("P1M"));

        return [
            "category_id" => $this->randomCategoryFunction(),
            "title" => Str::ucfirst($title),
            "slug" => Str::slug($title),
            "image_url" => $this->randomImageUrlFunction(),
            "from" => $from,
            "to" => $to,
            "application_deadline" => $applicationDeadline,
            "place" => $this->faker->address,
            "description" => $this->faker->realText(3000),
            "published" => $this->faker->boolean(80),
            "archived" => $this->faker->boolean(5),
            "tags" => $this->randomTagsFunction(),
        ];
    }

    /**
     * Creates function that returns random (existing) category id.
     *
     * @return callable
     */
    private function randomCategoryFunction(): callable
    {
        return function () {
            return Category::query()
                ->inRandomOrder()
                ->first()
                ->id;
        };
    }

    /**
     * Creates function that returns random (existing) tags names.
     *
     * @return callable
     */
    private function randomTagsFunction(): callable
    {
        return function () {
            return Tag::query()
                ->inRandomOrder()
                ->limit(mt_rand(3, 7))
                ->get()
                ->pluck("name")
                ->toArray();
        };
    }

    /**
     * Creates function that randomly returns image url or null.
     *
     * @return callable
     */
    private function randomImageUrlFunction(): callable
    {
        return function () {
            if (rand(0, 1)) {
                return $this->faker->imageUrl(300, 300);
            }

            return null;
        };
    }
}
