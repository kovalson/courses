<?php
declare(strict_types=1);
namespace App\Observers;

use App\Models\Tag;

class TagObserver
{
    /**
     * Handle the Tag "created" event.
     *
     * @param Tag $tag
     * @return void
     */
    public function created(Tag $tag): void
    {
        // Whenever the tag is created by an authorized user
        // it should automatically get published.
        if (auth()->check()) {
            $tag->update([
                Tag::PUBLISHED => true,
            ]);
        }
    }

    /**
     * Handle the Tag "saving" event.
     *
     * @param Tag $tag
     * @return void
     */
    public function saving(Tag $tag): void
    {
        $tag->generateSlug($tag->name);
    }
}
