<?php
declare(strict_types=1);
namespace App\Observers;

use App\Models\Category;

class CategoryObserver
{
    /**
     * Handle the category "saving" event.
     *
     * @param Category $category
     * @return void
     */
    public function saving(Category $category): void
    {
        $category->generateSlug();
    }
}
