<?php
declare(strict_types=1);
namespace App\Observers;

use App\Models\Course;

class CourseObserver
{
    /**
     * Handle the course "saving" event.
     *
     * @param Course $course
     * @return void
     */
    public function saving(Course $course): void
    {
        $course->generateSlug($course->title);
    }
}
