<?php
declare(strict_types=1);
namespace App\Http\Requests\Course;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest
 *
 * @package App\Http\Requests\Course
 * @property-read string $title
 * @property-read int $category_id
 * @property-read string $image_url
 * @property-read string $from
 * @property-read string $to
 * @property-read string $application_deadline
 * @property-read string $place
 * @property-read string $description
 * @property-read string[] $tags
 */
abstract class BaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "title" => "required|string|max:255",
            "category_id" => "required|exists:categories,id",
            "image_url" => "nullable|url",
            "from" => "required|regex:/^[0-9]{2}\.[0-9]{2}\.[0-9]{4}\s[0-9]{2}:[0-9]{2}$/",
            "to" => "required|regex:/^[0-9]{2}\.[0-9]{2}\.[0-9]{4}\s[0-9]{2}:[0-9]{2}$/",
            "application_deadline" => "required|regex:/^[0-9]{2}\.[0-9]{2}\.[0-9]{4}\s[0-9]{2}:[0-9]{2}$/",
            "place" => "required|string|max:255",
            "description" => "nullable|string",
            "tags" => "nullable|array",
            "tags.*" => "string",
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            "title.required" => __("Należy podać nazwę szkolenia"),
            "title.string" => __("Nazwa szkolenia musi być ciągiem znaków"),
            "title.max" => __("Nazwa szkolenia nie może mieć więcej niż :max znaków"),
            "category_id.required" => __("Należy wybrać kategorię szkolenia"),
            "category_id.exists" => __("Wybrana kategoria szkolenia nie istnieje"),
            "image_url.url" => __("Podany adres obrazka nie jest prawidłowym adresem URL"),
            "from.required" => __("Należy podać datę rozpoczęcia szkolenia"),
            "from.regex" => __("Data rozpoczęcia szkolenia musi być formatu daty i godziny"),
            "to.required" => __("Należy podać datę zakończenia szkolenia"),
            "to.regex" => __("Data zakończenia szkolenia musi być formatu daty i godziny"),
            "application_deadline.required" => __("Należy podać ostatni dzień składania zgłoszeń"),
            "application_deadline.regex" => __("Ostatni dzień składania zgłoszeń musi być formatu daty i godziny"),
            "place.required" => __("Należy podać miejsce szkolenia"),
            "place.string" => __("Miejsce szkolenia musi być ciągiem znaków"),
            "place.max" => __("Miejsce szkolenia nie może mieć więcej niż :max znaków"),
            "description.text" => __("Opis szkolenia musi być tekstem"),
            "tags.array" => __("Tagi muszą być tablicą wartości"),
            "tags.*.string" => __("Tag musi być tekstem"),
        ];
    }
}
