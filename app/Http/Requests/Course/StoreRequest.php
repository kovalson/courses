<?php
declare(strict_types=1);
namespace App\Http\Requests\Course;

class StoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return parent::rules() + [
            "captcha" => "present",
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return parent::messages() + [
            "captcha.present" => __("Należy zaznaczyć checkbox CAPTCHA"),
        ];
    }
}
