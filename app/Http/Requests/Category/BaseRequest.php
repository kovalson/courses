<?php
declare(strict_types=1);
namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class BaseRequest
 *
 * @package App\Http\Requests\Category
 * @property-read string $name
 * @property-read int $category_id
 */
abstract class BaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "name" => "required|string",
            "category_id" => "nullable|exists:categories,id",
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            "name.required" => __("Należy podać nazwę kategorii"),
            "name.string" => __("Nazwa kategorii musi być ciągiem znaków"),
            "category_id.exists" => __("Wybrana kategoria nie istnieje"),
        ];
    }
}
