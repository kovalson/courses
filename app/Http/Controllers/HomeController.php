<?php
declare(strict_types=1);
namespace App\Http\Controllers;

use App\Repositories\Interfaces\CourseRepository;
use App\Services\Filters\CourseArchivingFilter;
use App\Services\Filters\CourseCategoryFilter;
use App\Services\Filters\CourseOrderFilter;
use App\Services\Filters\CoursePublicationFilter;
use App\Services\Filters\OrderDirectionFilter;
use Illuminate\Contracts\View\View;

class HomeController extends Controller
{
    /**
     * The course repository.
     *
     * @var CourseRepository
     */
    private $courseRepository;

    /**
     * HomeController constructor.
     *
     * @param CourseRepository $courseRepository
     * @return void
     */
    public function __construct(CourseRepository $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return View
     */
    public function index(): View
    {
        $courses = $this->courseRepository->all();

        $filters = $this->getCourseFilters();

        return view("home", [
            "courses" => $courses,
            "filters" => $filters,
        ]);
    }

    /**
     * Returns all filters to scope the courses.
     *
     * @return array
     */
    private function getCourseFilters(): array
    {
        $baseFilters = $this->getBaseFilters();
        $administratorFilters = $this->getAdministratorFilters();

        return array_merge($baseFilters, $administratorFilters);
    }

    /**
     * Returns base filters available for anyone.
     *
     * @return CoursePublicationFilter[]
     */
    private function getBaseFilters(): array
    {
        return [
            new CourseOrderFilter(),
            new OrderDirectionFilter(),
            new CourseCategoryFilter(),
        ];
    }

    /**
     * Returns filters available only for administrators.
     *
     * @return CoursePublicationFilter[]
     */
    private function getAdministratorFilters(): array
    {
        if (auth()->check()) {
            return [
                new CoursePublicationFilter(),
                new CourseArchivingFilter(),
            ];
        }

        return [];
    }
}
