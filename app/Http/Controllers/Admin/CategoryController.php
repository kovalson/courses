<?php
declare(strict_types=1);
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Repositories\Interfaces\CategoryRepository;
use App\Repositories\Interfaces\CourseRepository;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * The category repository.
     *
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * The course repository.
     *
     * @var CourseRepository
     */
    private $courseRepository;

    /**
     * CategoryController constructor.
     *
     * @param CategoryRepository $categoryRepository
     * @param CourseRepository $courseRepository
     * @return void
     */
    public function __construct(CategoryRepository $categoryRepository, CourseRepository $courseRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->courseRepository = $courseRepository;
    }

    /**
     * Display the categories list.
     *
     * @return View
     */
    public function index(): View
    {
        $categories = $this->categoryRepository->structured();

        return view("category.auth.index", [
            "categories" => $categories,
        ]);
    }

    /**
     * Show the create form.
     *
     * @return View
     */
    public function create(): View
    {
        $baseCategories = $this->categoryRepository
            ->getBaseCategories()
            ->pluck("name", "id")
            ->toArray();

        $baseCategories = [null => __("Brak")] + $baseCategories;

        return view("category.auth.create", [
            "baseCategories" => $baseCategories,
        ]);
    }

    /**
     * Store the newly created category.
     *
     * @param StoreRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRequest $request): RedirectResponse
    {
        $this->categoryRepository->createFromRequest($request);

        return redirect()
            ->route("categories.index")
            ->with("success", __("Kategoria została utworzona."));
    }

    /**
     * Show the edit form.
     *
     * @param string $slug
     * @return View
     */
    public function edit(string $slug): View
    {
        $baseCategories = $this->categoryRepository
            ->getBaseCategories()
            ->pluck("name", "id")
            ->toArray();

        $baseCategories = array_merge([null => __("Brak")], $baseCategories);

        $category = $this->categoryRepository->getBySlug($slug);

        return view("category.auth.edit", [
            "baseCategories" => $baseCategories,
            "category" => $category,
        ]);
    }

    /**
     * Update the category.
     *
     * @param UpdateRequest $request
     * @param string $slug
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, string $slug): RedirectResponse
    {
        $category = $this->categoryRepository->updateFromRequest($slug, $request);

        return redirect()
            ->route("categories.edit", ["slug" => $category->slug])
            ->with("success", __("Kategoria została zapisana."));
    }

    /**
     * Moves the category up in order.
     *
     * @param string $slug
     * @return RedirectResponse
     */
    public function moveUp(string $slug): RedirectResponse
    {
        $this->categoryRepository->moveUp($slug);

        return redirect()
            ->back()
            ->with("success", __("Kategoria została przesunięta w górę."));
    }

    /**
     * Moves the category down in order.
     *
     * @param string $slug
     * @return RedirectResponse
     */
    public function moveDown(string $slug): RedirectResponse
    {
        $this->categoryRepository->moveDown($slug);

        return redirect()
            ->back()
            ->with("success", __("Kategoria została przesunięta w dół."));
    }

    /**
     * Remove the category.
     *
     * @param string $slug
     * @return RedirectResponse
     */
    public function destroy(string $slug): RedirectResponse
    {
        $this->categoryRepository->deleteOne($slug);

        return redirect()
            ->back()
            ->with("success", __("Kategoria została usunięta"));
    }
}
