<?php
declare(strict_types=1);
namespace App\Http\Controllers;

use App\Http\Requests\Course\StoreRequest;
use App\Http\Requests\Course\UpdateRequest;
use App\Repositories\Interfaces\CategoryRepository;
use App\Repositories\Interfaces\CourseRepository;
use App\Repositories\Interfaces\TagRepository;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class CourseController extends Controller
{
    /**
     * The category repository.
     *
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * The course repository.
     *
     * @var CourseRepository
     */
    private $courseRepository;

    /**
     * The tag repository.
     *
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * CourseController constructor.
     *
     * @param CategoryRepository $categoryRepository
     * @param CourseRepository $courseRepository
     * @param TagRepository $tagRepository
     * @return void
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        CourseRepository $courseRepository,
        TagRepository $tagRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->courseRepository = $courseRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * Display the course create form.
     *
     * @return View
     */
    public function create(): View
    {
        $categories = $this->categoryRepository->getGrouped();
        $tags = $this->tagRepository
            ->all()
            ->pluck("name", "name");

        return view("course.create", [
            "categoriesOptions" => $categories,
            "tags" => $tags,
        ]);
    }

    /**
     * Stores new course in the database.
     *
     * @param StoreRequest $request
     * @return RedirectResponse|View
     */
    public function store(StoreRequest $request)
    {
        // We create the course
        $created = $this->courseRepository->createFromRequest($request);

        if ($created) {
            return view("course.201", [
                "title" => $request->title,
            ]);
        }

        return redirect()->back();
    }

    /**
     * Show the course details.
     *
     * @param string $slug
     * @return View
     */
    public function show(string $slug): View
    {
        $course = $this->courseRepository->getBySlug($slug);

        if (is_null($course)) {
            return view("course.404");
        }

        return view("course.show", [
            "course" => $course,
        ]);
    }

    /**
     * Show the edit form for course.
     *
     * @param string $slug
     * @return View
     */
    public function edit(string $slug): View
    {
        $course = $this->courseRepository->getBySlug($slug);
        $categories = $this->categoryRepository->getGrouped();
        $tags = $this->tagRepository
            ->all()
            ->pluck("name", "name");

        return view("course.edit", [
            "course" => $course,
            "categoriesOptions" => $categories,
            "tags" => $tags,
        ]);
    }

    /**
     * Updates the course.
     *
     * @param UpdateRequest $request
     * @param string $slug
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, string $slug): RedirectResponse
    {
        $this->courseRepository->updateFromRequest($slug, $request);

        return redirect()
            ->back()
            ->with("success", __("Zmiany zostały zapisane."));
    }

    /**
     * Archives the course.
     *
     * @param string $slug
     * @return RedirectResponse
     */
    public function archive(string $slug): RedirectResponse
    {
        $this->courseRepository->update($slug, [
            "archived" => true,
        ]);

        return redirect()
            ->back()
            ->with("success", __("Szkolenie zostało zarchiwizowane."));
    }

    /**
     * Restores the course from archive.
     *
     * @param string $slug
     * @return RedirectResponse
     */
    public function restore(string $slug): RedirectResponse
    {
        $this->courseRepository->update($slug, [
            "archived" => false,
        ]);

        return redirect()
            ->back()
            ->with("success", __("Szkolenie zostało zarchiwizowane."));
    }

    /**
     * Publishes the course.
     *
     * @param string $slug
     * @return RedirectResponse
     */
    public function publish(string $slug): RedirectResponse
    {
        $this->courseRepository->update($slug, [
            "published" => true,
        ]);

        return redirect()
            ->back()
            ->with("success", __("Szkolenie zostało opublikowane."));
    }

    /**
     * Hides the course.
     *
     * @param string $slug
     * @return RedirectResponse
     */
    public function hide(string $slug): RedirectResponse
    {
        $this->courseRepository->update($slug, [
            "published" => false,
        ]);

        return redirect()
            ->back()
            ->with("success", __("Szkolenie zostało ukryte."));
    }

    /**
     * Removes course from the database.
     *
     * @param string $slug
     * @return RedirectResponse
     */
    public function destroy(string $slug): RedirectResponse
    {
        // We delete the course
        $deleted = $this->courseRepository->deleteOne($slug);

        // The return message
        $message = $deleted
            ? __("Szkolenie zostało usunięte.")
            : __("Wystąpił błąd podczas usuwania szkolenia.");

        // The return message type
        $messageType = $deleted
            ? "success"
            : "danger";

        return redirect()
            ->back()
            ->with($messageType, $message);
    }
}
