<?php
declare(strict_types=1);
namespace App\Http\Controllers;

use App\Repositories\Interfaces\CategoryRepository;
use App\Repositories\Interfaces\CourseRepository;
use App\Services\Filters\CategoryOrderFilter;
use App\Services\Filters\CourseOrderFilter;
use App\Services\Filters\OrderDirectionFilter;
use Illuminate\Contracts\View\View;

class CategoryController extends Controller
{
    /**
     * The category repository.
     *
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * The course repository.
     *
     * @var CourseRepository
     */
    private $courseRepository;

    /**
     * CategoryController constructor.
     *
     * @param CategoryRepository $categoryRepository
     * @param CourseRepository $courseRepository
     * @return void
     */
    public function __construct(CategoryRepository $categoryRepository, CourseRepository $courseRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->courseRepository = $courseRepository;
    }

    /**
     * Shows courses that belong to category.
     *
     * @param string $slug
     * @return View
     */
    public function courses(string $slug): View
    {
        $category = $this->categoryRepository->getBySlug($slug);

        if (is_null($category)) {
            return view("category.404");
        }

        $courses = $this->courseRepository->getByCategoryId($category->id);

        $filters = [
            new CourseOrderFilter(),
            new OrderDirectionFilter(),
        ];

        return view("category.courses", [
            "category" => $category,
            "courses" => $courses,
            "filters" => $filters,
        ]);
    }

    /**
     * Shows the categories that belong to category.
     *
     * @param string $slug
     * @return View
     */
    public function categories(string $slug): View
    {
        $category = $this->categoryRepository->getBySlug($slug);

        if (is_null($category)) {
            return view("category.404");
        }

        $categories = $this->categoryRepository->getCategoriesByCategoryId($category->id);

        $filters = [
            new CategoryOrderFilter(),
            new OrderDirectionFilter(),
        ];

        return view("category.categories", [
            "category" => $category,
            "categories" => $categories,
            "filters" => $filters,
        ]);
    }
}
