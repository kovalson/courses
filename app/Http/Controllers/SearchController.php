<?php
declare(strict_types=1);
namespace App\Http\Controllers;

use App\Repositories\Interfaces\CategoryRepository;
use App\Repositories\Interfaces\CourseRepository;
use App\Repositories\Interfaces\TagRepository;
use App\Services\Filters\CategoryOrderFilter;
use App\Services\Filters\CourseArchivingFilter;
use App\Services\Filters\CourseCategoryFilter;
use App\Services\Filters\CourseOrderFilter;
use App\Services\Filters\CoursePublicationFilter;
use App\Services\Filters\OrderDirectionFilter;
use App\Services\Filters\TagOrderFilter;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * The category repository.
     *
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * The course repository.
     *
     * @var CourseRepository
     */
    private $courseRepository;

    /**
     * The tag repository.
     *
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * SearchController constructor.
     *
     * @param CategoryRepository $categoryRepository
     * @param CourseRepository $courseRepository
     * @param TagRepository $tagRepository
     * @return void
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        CourseRepository $courseRepository,
        TagRepository $tagRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->courseRepository = $courseRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * Show the search results page.
     *
     * @param Request $request
     * @return RedirectResponse|View
     */
    public function __invoke(Request $request)
    {
        $queryText = $request->get("q");
        $searchIn = $request->get("search_in");

        if (is_null($queryText)) {
            return redirect()->route("home");
        }

        $data = [];

        switch ($searchIn) {
            case "categories":
                $data["categories"] = $this->categoryRepository->search($queryText);
                $data["filters"] = [
                    new CategoryOrderFilter(),
                    new OrderDirectionFilter(),
                ];
                break;
            case "tags":
                $data["tags"] = $this->tagRepository->search($queryText);
                $data["filters"] = [
                    new TagOrderFilter(),
                    new OrderDirectionFilter(),
                ];
                break;
            case "courses":
            default:
                $data["courses"] = $this->courseRepository->search($queryText);
                $data["filters"] = [
                    new CourseOrderFilter(),
                    new OrderDirectionFilter(),
                    new CoursePublicationFilter(),
                    new CourseArchivingFilter(),
                    new CourseCategoryFilter(),
                ];
                break;
        }

        return view("search.index", $data);
    }
}
