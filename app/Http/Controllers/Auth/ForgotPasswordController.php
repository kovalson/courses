<?php
declare(strict_types=1);
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    /**
     * Validate the email for the given request.
     *
     * @param Request $request
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $request->validate([
            "email" => "required|email",
            "g-recaptcha-response" => "required|captcha"
        ], [
            "email.required" => __("Należy podać adres e-mail."),
            "email.email" => __("Podany adres e-mail jest nieprawidłowy."),
            "g-recaptcha-response.required" => __("Należy uzupełnić CAPTCHA."),
            "g-recaptcha-response.captcha" => __("Nieprawidłowa wartość CAPTCHA."),
        ]);
    }
}
