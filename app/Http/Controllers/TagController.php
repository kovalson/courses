<?php
declare(strict_types=1);
namespace App\Http\Controllers;

use App\Repositories\Interfaces\CourseRepository;
use App\Repositories\Interfaces\TagRepository;
use App\Services\Filters\CourseOrderFilter;
use App\Services\Filters\OrderDirectionFilter;
use Illuminate\Contracts\View\View;

class TagController extends Controller
{
    /**
     * The tag repository.
     *
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * The course repository.
     *
     * @var CourseRepository
     */
    private $courseRepository;

    /**
     * TagController constructor.
     *
     * @param TagRepository $tagRepository
     * @param CourseRepository $courseRepository
     * @return void
     */
    public function __construct(TagRepository $tagRepository, CourseRepository $courseRepository)
    {
        $this->tagRepository = $tagRepository;
        $this->courseRepository = $courseRepository;
    }

    /**
     * Returns the tag page.
     *
     * @param string $tagSlug
     * @return View
     */
    public function __invoke(string $tagSlug): View
    {
        $tag = $this->tagRepository->getBySlug($tagSlug);

        if (is_null($tag)) {
            return view("tag.404");
        }

        $courses = $this->courseRepository->getByAnyTags([$tag->name]);

        $filters = [
            new CourseOrderFilter(),
            new OrderDirectionFilter(),
        ];

        return view("tag.index", [
            "tag" => $tag,
            "courses" => $courses,
            "filters" => $filters,
        ]);
    }
}
