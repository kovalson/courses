<?php
declare(strict_types=1);
namespace App\Http\Middleware;

use Closure;
use Config;
use Illuminate\Http\Request;

class AppVersionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $metadata = json_decode(file_get_contents(base_path("package.json")));
        Config::set("APP_VERSION", $metadata->version);

        return $next($request);
    }
}
