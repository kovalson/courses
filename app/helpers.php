<?php

use App\Models\Course;
use App\Services\Filters\Filter;
use Illuminate\Support\Carbon;

if (!function_exists("mb_lcfirst")) {
    /**
     * Converts any multibyte first letter to lower case in given string.
     * This helper is different from lcfirst() in such way that the former
     * doesn't work for multibyte characters such as diacritics.
     *
     * @param string $text
     * @return string
     */
    function mb_lcfirst(string $text): string
    {
        return mb_strtolower(mb_substr($text, 0, 1)) . mb_substr($text, 1);
    }
}

if (!function_exists("get_alert_icon")) {
    /**
     * Returns icon class name for given alert type.
     * The class refers to Font awesome icons classes. If given type is not recognized
     * the function returns "fas fa-question" as question mark icon.
     *
     * @param string $type
     * @return string
     */
    function get_alert_icon(string $type): string
    {
        $icons = [
            "info" => "fas fa-info-circle",
            "success" => "fas fa-check",
            "warning" => "fas fa-exclamation-triangle",
            "danger" => "fas fa-exclamation-circle",
            "error" => "fas fa-exclamation-circle",
        ];

        return isset($icons[$type]) ? $icons[$type] : "fas fa-question";
    }
}

if (!function_exists("setting")) {
    /**
     * Returns the setting value.
     *
     * @param string $key
     * @return mixed
     */
    function setting(string $key)
    {
        return config("settings." . $key);
    }
}

if (!function_exists("pretty_date")) {
    /**
     * Returns pretty-formatted date string using given format.
     *
     * @param Carbon|string|null $date
     * @param string $format
     * @return string|null
     */
    function pretty_date($date, string $format = "l, jS F Y, \g\o\d\z\. h:i"): ?string
    {
        if (is_null($date)) {
            return null;
        } else if (is_string($date)) {
            $date = Carbon::create($date);
        }

        return $date->translatedFormat($format);
    }
}

if (!function_exists("input_date")) {
    /**
     * Returns input-formatted date.
     *
     * @param $date
     * @return string|null
     */
    function input_date($date): ?string
    {
        return pretty_date($date, "d.m.Y h:i");
    }
}

if (!function_exists("timetable_date")) {
    /**
     * Returns pretty-formatted date string with custom format.
     *
     * @see pretty_date()
     * @param $date
     * @param string $format
     * @return string|null
     */
    function timetable_date($date, string $format = "l, jS F Y, h:i"): ?string
    {
        return pretty_date($date, $format);
    }
}

if (!function_exists("set_filter_url")) {
    /**
     * Generates URL for filter value.
     *
     * @param string $key
     * @param string|int $value
     * @return string
     */
    function set_filter_url(string $key, $value): string
    {
        return request()->fullUrlWithQuery([
            $key => $value,
            "page" => null,
        ]);
    }
}

if (!function_exists("get_filter_value")) {
    /**
     * Returns the filter value.
     *
     * @param string $filterClass
     * @return mixed
     */
    function get_filter_value(string $filterClass)
    {
        /** @var Filter $filter */
        $filter = new $filterClass();

        return $filter->getValue();
    }
}

if (!function_exists("get_course_image_url")) {
    /**
     * Returns image URL for given course.
     * If image doesn't exist returns URL to placeholder.
     *
     * @param Course $course
     * @return string
     */
    function get_course_image_url(Course $course): string
    {
        if ($course->image_url) {
            return $course->image_url;
        }

        return asset("img/placeholder-image.png");
    }
}
