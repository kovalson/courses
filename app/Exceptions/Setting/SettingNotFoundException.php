<?php
declare(strict_types=1);
namespace App\Exceptions\Setting;

use Exception;
use Illuminate\Http\Response;

class SettingNotFoundException extends Exception
{
    /**
     * SettingNotFoundException constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct(__("Nie znaleziono ustawienia w bazie."), Response::HTTP_NOT_FOUND);
    }
}
