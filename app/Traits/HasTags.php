<?php
declare(strict_types=1);
namespace App\Traits;

use App\Models\Tag;
use ArrayAccess;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Collection;

trait HasTags
{
    /**
     * Boot the trait.
     *
     * @return void
     */
    public static function bootHasTags(): void
    {
        self::created(function (Model $model) {
            /** @var HasTags $model */
            if (count($model->queuedTags) > 0) {
                $model->attachTags($model->queuedTags);

                $model->queuedTags = [];
            }
        });

        self::deleted(function (Model $model) {
            /** @var HasTags $model */
            $tags = $model->tags()->get();

            $model->detachTags($tags);
        });
    }

    /**
     * Converts given values to collection of tag objects.
     *
     * @param $values
     * @return Collection
     */
    protected static function convertToTags($values): Collection
    {
        return collect($values)->map(function ($value) {
            if ($value instanceof Tag) {
                return $value;
            }

            return Tag::findFromString($value);
        });
    }

    /**
     * The tags to be synchronized.
     *
     * @var array
     */
    protected $queuedTags = [];

    /**
     * Set the "tags" attribute.
     *
     * @param array $tags
     * @return void
     */
    public function setTagsAttribute($tags = []): void
    {
        if (!$this->exists) {
            $this->queuedTags = $tags;

            return;
        }

        $this->syncTags($tags);
    }

    /**
     * Returns the related tags.
     *
     * @return MorphToMany
     */
    public function tags(): MorphToMany
    {
        return $this->morphToMany(Tag::class, "taggable");
    }

    /**
     * Synchronizes tags.
     *
     * @param ArrayAccess|array $tags
     * @return $this
     */
    public function syncTags(array $tags): self
    {
        $tags = collect(Tag::findOrNew($tags));

        $this->tags()->sync($tags->pluck("id")->toArray());

        return $this;
    }

    /**
     * Attaches tags to the model.
     *
     * @param ArrayAccess|array|Tag $tags
     * @return $this
     */
    public function attachTags($tags): self
    {
        $tags = collect(Tag::findOrCreate($tags));

        $this->tags()->syncWithoutDetaching($tags->pluck("id")->toArray());

        return $this;
    }

    /**
     * Detaches given tags from the model.
     *
     * @param ArrayAccess|array $tags
     * @return $this
     */
    public function detachTags(array $tags): self
    {
        $tags = self::convertToTags($tags);

        collect($tags)
            ->filter()
            ->each(function (Tag $tag) {
                $this->tags()->detach($tag);
            });

        return $this;
    }
}
