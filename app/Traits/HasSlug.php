<?php
declare(strict_types=1);
namespace App\Traits;

use Illuminate\Support\Str;

trait HasSlug
{
    /**
     * Generate the slug based on given value.
     *
     * @param string $value
     * @param string $primaryKey
     * @param string $slugKey
     * @return void
     */
    public function generateSlug(string $value, string $primaryKey = "id", string $slugKey = "slug"): void {
        // We create the base slug
        $slug = Str::slug($value);

        // We check whether the base slug is already in use
        $slugInUse = self::query()
            ->where($slugKey, $slug)
            ->where($primaryKey, "!=", $this->{$primaryKey})
            ->exists();

        // If the slug is already in use we need to
        // suffix it with an order value
        if ($slugInUse) {
            // We check how many more slugs of the kind
            // are already in use
            $slugsOfTheKind = self::query()
                ->where($slugKey, "LIKE", $slug . "-" . "%")
                ->where($primaryKey, "!=", $this->{$primaryKey})
                ->count();

            // We increment the count as this slug
            // is going to be the next one
            $slugsOfTheKind++;

            // We append the count to the slug
            $slug = $slug . "-" . $slugsOfTheKind;
        }

        // We set the course slug
        $this->slug = $slug;
    }
}
