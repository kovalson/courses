<?php
declare(strict_types=1);
namespace App\Services\SidebarMenu;

class SidebarMenuItem
{
    /**
     * The display name of the item.
     *
     * @var string
     */
    public $name;

    /**
     * The category slug.
     *
     * @var string
     */
    public $slug;

    /**
     * The item URL.
     *
     * @var string
     */
    public $url;

    /**
     * URL to subcategories of item.
     *
     * @var string
     */
    public $urlCategories;

    /**
     * Array of sub-items.
     *
     * @var SidebarMenuItem[]
     */
    public $items;

    /**
     * Whether the item has more items than limited.
     *
     * @var bool
     */
    private $hasMoreItems;

    /**
     * SidebarMenuItem constructor.
     *
     * @param string $name
     * @param string $slug
     * @param array $items
     * @param bool $hasMore
     */
    public function __construct(string $name, string $slug, array $items = [], bool $hasMore = false)
    {
        $url = route("category.courses", $slug);
        $urlCategories = route("category.categories", $slug);

        $this->name = $name;
        $this->slug = $slug;
        $this->url = $url;
        $this->urlCategories = $urlCategories;
        $this->items = $items;
        $this->hasMoreItems = $hasMore;
    }

    /**
     * Checks whether the menu item has any sub-items.
     *
     * @return bool
     */
    public function hasItems(): bool
    {
        return !empty($this->items);
    }

    /**
     * Checks whether the menu item has more sub-items than
     * displayed (limited).
     *
     * @return bool
     */
    public function hasMore(): bool
    {
        return $this->hasMoreItems;
    }
}
