<?php
declare(strict_types=1);
namespace App\Services\SidebarMenu;

use App\Models\Category;
use App\Models\Setting;

class SidebarMenuService
{
    /**
     * The menu items.
     *
     * @var SidebarMenuItem[]
     */
    public $items;

    /**
     * Callable for category mapping.
     *
     * @var callable
     */
    private $categoryMapMethod;

    /**
     * The limit of items in sidebar menu.
     *
     * @var int
     */
    private $menuItemsLimit;

    /**
     * SidebarMenuService constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->menuItemsLimit = setting(Setting::MENU_ITEMS_LIMIT);

        $this->categoryMapMethod = $this->categoryMapMethod();

        $this->items = $this->createMenuItems();
    }

    /**
     * Checks whether the menu is empty (has no items).
     *
     * @return bool
     */
    public function isEmpty(): bool
    {
        return empty($this->items);
    }

    /**
     * Checks whether the menu item has any sub-items.
     *
     * @return bool
     */
    public function hasItems(): bool
    {
        return !$this->isEmpty();
    }

    /**
     * Creates array of menu items.
     *
     * @return array
     */
    private function createMenuItems(): array
    {
        return Category::query()
            ->whereNull("category_id")
            ->with("subcategories")
            ->orderBy("position")
            ->get()
            ->map($this->categoryMapMethod)
            ->toArray();
    }

    /**
     * Creates a menu item.
     *
     * @param Category $category
     * @return SidebarMenuItem
     */
    private function createMenuItem(Category $category): SidebarMenuItem
    {
        $name = $category->name;
        $slug = $category->slug;

        $hasMore = $this->hasMoreItems($category);

        $subcategories = $category->subcategories()
            ->with("subcategories")
            ->orderBy("position")
            ->limit($this->menuItemsLimit)
            ->get()
            ->map($this->categoryMapMethod)
            ->toArray();

        return new SidebarMenuItem($name, $slug, $subcategories, $hasMore);
    }

    /**
     * Returns a callable for category mapping.
     *
     * @return callable
     */
    private function categoryMapMethod(): callable
    {
        return function (Category $category) {
            return $this->createMenuItem($category);
        };
    }

    /**
     * Checks whether given category has more sub-categories
     * than what is the limit.
     *
     * @param Category $category
     * @return bool
     */
    private function hasMoreItems(Category $category): bool
    {
        $total = $category->subcategories()->count();

        return ($total > $this->menuItemsLimit);
    }
}
