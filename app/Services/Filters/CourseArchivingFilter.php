<?php
declare(strict_types=1);
namespace App\Services\Filters;

class CourseArchivingFilter extends Filter
{
    /**
     * Shows both active and archived courses.
     *
     * @type null
     */
    const ALL = -1;

    /**
     * Shows only archived courses.
     *
     * @type int
     */
    const ARCHIVED = 0;

    /**
     * Shows only active courses.
     *
     * @type int
     */
    const ACTIVE = 1;

    /**
     * Returns the filter request key.
     *
     * @return string
     */
    public function getKey(): string
    {
        return "archived";
    }

    /**
     * Returns the icon associated with the filter.
     *
     * @return string
     */
    public function getIcon(): string
    {
        return "fas fa-archive";
    }

    /**
     * Returns the description label.
     *
     * @return string
     */
    public function getLabel(): string
    {
        return __("Archiwizacja");
    }

    /**
     * Returns options to choose from.
     *
     * @return array
     */
    public function getAvailableOptions(): array
    {
        return [
            self::ALL => __("Wszystkie"),
            self::ACTIVE => __("Tylko aktywne"),
            self::ARCHIVED => __("Tylko archiwizowane"),
        ];
    }

    /**
     * Returns the default option.
     *
     * @return int
     */
    public function getDefaultOption(): int
    {
        return self::ALL;
    }

    /**
     * Whether the filter can be applied.
     *
     * @return bool
     */
    public function isApplicable(): bool
    {
        return auth()->check();
    }

    /**
     * Casts the value to given type.
     *
     * @param string $value
     * @return int
     */
    protected function cast(string $value): int
    {
        return (int) $value;
    }
}
