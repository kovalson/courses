<?php
declare(strict_types=1);
namespace App\Services\Filters;

abstract class OrderFilter extends Filter
{
    /**
     * Returns the filter request key.
     *
     * @return string
     */
    public function getKey(): string
    {
        return "order";
    }

    /**
     * Returns the icon associated with the filter.
     *
     * @return string
     */
    public function getIcon(): string
    {
        return "fas fa-sort-amount-down-alt";
    }
}
