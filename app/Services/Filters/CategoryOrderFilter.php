<?php
declare(strict_types=1);
namespace App\Services\Filters;

class CategoryOrderFilter extends OrderFilter
{
    /**
     * Order by category name.
     *
     * @type string
     */
    const NAME = "name";

    /**
     * Returns the description label.
     *
     * @return string
     */
    public function getLabel(): string
    {
        return __("Sortowanie");
    }

    /**
     * Returns options to choose from.
     *
     * @return array
     */
    public function getAvailableOptions(): array
    {
        return [
            self::NAME => __("Nazwa"),
        ];
    }

    /**
     * Returns the default option.
     *
     * @return string
     */
    public function getDefaultOption(): string
    {
        return self::NAME;
    }
}
