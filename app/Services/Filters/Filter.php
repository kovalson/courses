<?php
declare(strict_types=1);
namespace App\Services\Filters;

abstract class Filter
{
    /**
     * Select type filter with dropdown menu component.
     *
     * @type string
     */
    const TYPE_DROPDOWN = "dropdown";

    /**
     * Returns the filter request key.
     *
     * @return string
     */
    public abstract function getKey(): string;

    /**
     * Returns the icon associated with the filter.
     *
     * @return string
     */
    public abstract function getIcon(): string;

    /**
     * Returns the description label.
     *
     * @return string
     */
    public abstract function getLabel(): string;

    /**
     * Returns options to choose from.
     *
     * @return array
     */
    public abstract function getAvailableOptions(): array;

    /**
     * Returns the default option.
     *
     * @return mixed
     */
    public abstract function getDefaultOption();

    /**
     * The current filter type.
     *
     * @var string
     */
    public $type = self::TYPE_DROPDOWN;

    /**
     * Returns current filter value.
     *
     * @return string|int
     */
    public function getValue()
    {
        $key = $this->getKey();
        $availableOptions = $this->getAvailableOptions();

        if (request()->has($key)) {
            $requestValue = request()->get($key);

            if (in_array($requestValue, array_keys($availableOptions))) {
                return $this->cast($requestValue);
            }
        }

        return $this->getDefaultOption();
    }

    /**
     * Returns label of current filter value.
     *
     * @return string
     */
    public function getValueLabel(): string
    {
        $options = $this->getAvailableOptions();
        $value = $this->getValue();

        return $options[$value];
    }

    /**
     * Checks if the filter value is different than default.
     *
     * @return bool
     */
    public function isSet(): bool
    {
        return ($this->getValue() !== $this->getDefaultOption());
    }

    /**
     * Checks if filter type of type dropdown.
     *
     * @return bool
     */
    public function isDropdown(): bool
    {
        return ($this->type === self::TYPE_DROPDOWN);
    }

    /**
     * Whether the filter can be applied.
     *
     * @return bool
     */
    public function isApplicable(): bool
    {
        return true;
    }

    /**
     * Casts the value to given type.
     *
     * @param string $value
     * @return string|int
     */
    protected function cast(string $value)
    {
        return (string) $value;
    }
}
