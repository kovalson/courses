<?php
declare(strict_types=1);
namespace App\Services\Filters;

class CoursePublicationFilter extends Filter
{
    /**
     * Show both published and unpublished courses.
     *
     * @type null
     */
    const ALL = -1;

    /**
     * Show only unpublished courses.
     *
     * @type int
     */
    const UNPUBLISHED = 0;

    /**
     * Show only published courses.
     *
     * @type int
     */
    const PUBLISHED = 1;

    /**
     * Returns the filter request key.
     *
     * @return string
     */
    public function getKey(): string
    {
        return "published";
    }

    /**
     * Returns the icon associated with the filter.
     *
     * @return string
     */
    public function getIcon(): string
    {
        return "far fa-eye";
    }

    /**
     * Returns the description label.
     *
     * @return string
     */
    public function getLabel(): string
    {
        return __("Widoczność szkolenia");
    }

    /**
     * Returns options to choose from.
     *
     * @return array
     */
    public function getAvailableOptions(): array
    {
        return [
            self::ALL => __("Wszystkie"),
            self::UNPUBLISHED => __("Nieopublikowane"),
            self::PUBLISHED => __("Opublikowane"),
        ];
    }

    /**
     * Returns the default option.
     *
     * @return int
     */
    public function getDefaultOption(): int
    {
        return self::ALL;
    }

    /**
     * Whether the filter can be applied.
     *
     * @return bool
     */
    public function isApplicable(): bool
    {
        return auth()->check();
    }

    /**
     * Casts the value to given type.
     *
     * @param string $value
     * @return int
     */
    protected function cast(string $value): int
    {
        return (int) $value;
    }
}
