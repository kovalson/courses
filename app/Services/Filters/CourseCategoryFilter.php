<?php
declare(strict_types=1);
namespace App\Services\Filters;

class CourseCategoryFilter extends Filter
{
    /**
     * Show all courses with or without category.
     *
     * @type int
     */
    const ALL = -1;

    /**
     * Shows only courses without category assigned.
     *
     * @type int
     */
    const WITHOUT_CATEGORY = 0;

    /**
     * Shows only courses with category assigned.
     *
     * @type int
     */
    const WITH_CATEGORY = 1;

    /**
     * Returns the filter request key.
     *
     * @return string
     */
    public function getKey(): string
    {
        return "course_category";
    }

    /**
     * Returns the icon associated with the filter.
     *
     * @return string
     */
    public function getIcon(): string
    {
        return "fas fa-layer-group";
    }

    /**
     * Returns the description label.
     *
     * @return string
     */
    public function getLabel(): string
    {
        return __("Kategoryzacja");
    }

    /**
     * Returns options to choose from.
     *
     * @return array
     */
    public function getAvailableOptions(): array
    {
        return [
            self::ALL => __("Wszystkie"),
            self::WITHOUT_CATEGORY => __("Bez kategorii"),
            self::WITH_CATEGORY => __("Z przypisaną kategorią"),
        ];
    }

    /**
     * Returns the default option.
     *
     * @return mixed
     */
    public function getDefaultOption(): int
    {
        return self::ALL;
    }

    /**
     * Casts the value to given type.
     *
     * @param string $value
     * @return string|int
     */
    protected function cast(string $value): int
    {
        return (int) $value;
    }
}
