<?php
declare(strict_types=1);
namespace App\Services\Filters;

class OrderDirectionFilter extends Filter
{
    /**
     * Returns the filter request key.
     *
     * @return string
     */
    public function getKey(): string
    {
        return "direction";
    }

    /**
     * Returns the icon associated with the filter.
     *
     * @return string
     */
    public function getIcon(): string
    {
        return "fas fa-sort";
    }

    /**
     * Returns the description label.
     *
     * @return string
     */
    public function getLabel(): string
    {
        return __("Porządek sortowania");
    }

    /**
     * Returns options to choose from.
     *
     * @return array
     */
    public function getAvailableOptions(): array
    {
        return [
            "asc" => __("Rosnąco"),
            "desc" => __("Malejąco"),
        ];
    }

    /**
     * Returns the default option.
     *
     * @return string
     */
    public function getDefaultOption(): string
    {
        return "asc";
    }
}
