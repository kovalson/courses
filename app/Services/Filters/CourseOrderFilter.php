<?php
declare(strict_types=1);
namespace App\Services\Filters;

class CourseOrderFilter extends OrderFilter
{
    /**
     * Order by application deadline.
     *
     * @type string
     */
    const APPLICATION_DEADLINE = "application_deadline";

    /**
     * Order by creation date.
     *
     * @type string
     */
    const CREATION_DATE = "created_at";

    /**
     * Order by course beginning date.
     *
     * @type string
     */
    const FROM_DATE = "from";

    /**
     * Order by course ending date.
     *
     * @type string
     */
    const TO_DATE = "to";

    /**
     * Returns the description label.
     *
     * @return string
     */
    public function getLabel(): string
    {
        return __("Sortowanie");
    }

    /**
     * Returns options to choose from.
     *
     * @return array
     */
    public function getAvailableOptions(): array
    {
        return [
            self::APPLICATION_DEADLINE => __("Ostatni dzień przyjmowania zgłoszeń"),
            self::CREATION_DATE => __("Data utworzenia"),
            self::FROM_DATE => __("Termin rozpoczęcia"),
            self::TO_DATE => __("Termin zakończenia"),
        ];
    }

    /**
     * Returns the default option.
     *
     * @return string
     */
    public function getDefaultOption(): string
    {
        return self::APPLICATION_DEADLINE;
    }
}
