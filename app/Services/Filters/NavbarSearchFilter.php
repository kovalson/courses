<?php
declare(strict_types=1);
namespace App\Services\Filters;

class NavbarSearchFilter extends Filter
{
    /**
     * Search within courses only.
     *
     * @type string
     */
    const IN_COURSES = "courses";

    /**
     * Search within courses categories only.
     *
     * @type string
     */
    const IN_CATEGORIES = "categories";

    /**
     * Search within courses tags only.
     *
     * @type string
     */
    const IN_TAGS = "tags";

    /**
     * Returns the filter request key.
     *
     * @return string
     */
    public function getKey(): string
    {
        return "search_in";
    }

    /**
     * Returns the icon associated with the filter.
     *
     * @return string
     */
    public function getIcon(): string
    {
        return "";
    }

    /**
     * Returns the description label.
     *
     * @return string
     */
    public function getLabel(): string
    {
        return __("Szukaj w");
    }

    /**
     * Returns options to choose from.
     *
     * @return array
     */
    public function getAvailableOptions(): array
    {
        return [
            self::IN_COURSES => __("Szkolenia"),
            self::IN_CATEGORIES => __("Kategorie"),
            self::IN_TAGS => __("Tagi"),
        ];
    }

    /**
     * Returns the default option.
     *
     * @return string
     */
    public function getDefaultOption(): string
    {
        return self::IN_COURSES;
    }
}
