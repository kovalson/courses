<?php
declare(strict_types=1);
namespace App\Models;

use App\Services\Filters\CourseOrderFilter;
use App\Services\Filters\OrderDirectionFilter;
use App\Traits\HasSlug;
use App\Traits\HasTags;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Support\Str;

/**
 * App\Models\Course
 *
 * @property int $id
 * @property int|null $category_id
 * @property string $title
 * @property string $slug
 * @property string|null $image_url
 * @property string $from
 * @property string $to
 * @property string $application_deadline
 * @property string $place
 * @property string|null $description
 * @property int $published
 * @property int $archived
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Category|null $category
 * @property Collection|Tag[] $tags
 * @property-read int|null $tags_count
 * @method static Builder|Course active(int $archived = -1)
 * @method static Builder|Course newModelQuery()
 * @method static Builder|Course newQuery()
 * @method static Builder|Course ordered()
 * @method static Builder|Course published(int $published = -1)
 * @method static Builder|Course query()
 * @method static Builder|Course whereApplicationDeadline($value)
 * @method static Builder|Course whereArchived($value)
 * @method static Builder|Course whereCategoryId($value)
 * @method static Builder|Course whereCreatedAt($value)
 * @method static Builder|Course whereDescription($value)
 * @method static Builder|Course whereFrom($value)
 * @method static Builder|Course whereId($value)
 * @method static Builder|Course whereImageUrl($value)
 * @method static Builder|Course wherePlace($value)
 * @method static Builder|Course wherePublished($value)
 * @method static Builder|Course whereSlug($value)
 * @method static Builder|Course whereTitle($value)
 * @method static Builder|Course whereTo($value)
 * @method static Builder|Course whereUpdatedAt($value)
 * @method static Builder|Course withAllTags($tags, ?string $type = null)
 * @method static Builder|Course withAllTagsOfAnyType($tags)
 * @method static Builder|Course withAnyTags($tags, ?string $type = null)
 * @method static Builder|Course withAnyTagsOfAnyType($tags)
 * @mixin Eloquent
 * @method static Builder|Course categorized(int $categorized = -1)
 */
class Course extends Model
{
    const ID                    = "id";
    const TITLE                 = "title";
    const SLUG                  = "slug";
    const CATEGORY_ID           = "category_id";
    const IMAGE_URL             = "image_url";
    const FROM                  = "from";
    const TO                    = "to";
    const APPLICATION_DEADLINE  = "application_deadline";
    const PLACE                 = "place";
    const DESCRIPTION           = "description";
    const PUBLISHED             = "published";
    const ARCHIVED              = "archived";
    const TAGS                  = "tags";

    use HasFactory;
    use HasSlug;
    use HasTags;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var string[]|bool
     */
    protected $guarded = [
        self::ID,
    ];

    /**
     * Returns category it belongs to.
     *
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class)
            ->withDefault([
                Category::NAME => "",
                Category::SLUG => "",
                Category::POSITION => 0,
                Category::CATEGORY_ID => null,
            ]);
    }

    /**
     * Scopes courses to only published courses.
     *
     * @param Builder $query
     * @param int $published
     * @return Builder
     */
    public function scopePublished(Builder $query, int $published = -1): Builder
    {
        if (auth()->check()) {
            if ($published >= 0) {
                return $query->where(self::PUBLISHED, $published);
            }

            return $query;
        }

        return $query->where(self::PUBLISHED, true);
    }

    /**
     * Scopes courses to not archived ones.
     *
     * @param Builder $query
     * @param int $archived
     * @return Builder
     */
    public function scopeActive(Builder $query, int $archived = -1): Builder
    {
        if (auth()->check()) {
            if ($archived >= 0) {
                return $query->where(self::ARCHIVED, !$archived);
            }

            return $query;
        }

        return $query->where(self::ARCHIVED, false);
    }

    /**
     * Orders the query using request params.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        $column = get_filter_value(CourseOrderFilter::class);
        $direction = get_filter_value(OrderDirectionFilter::class);

        return $query->orderBy($column, $direction);
    }

    /**
     * Scopes courses to those categorized, not categorized or both.
     *
     * @param Builder $query
     * @param int $categorized
     * @return Builder
     */
    public function scopeCategorized(Builder $query, int $categorized = -1): Builder
    {
        if ($categorized === 0) {
            return $query->whereNull(self::CATEGORY_ID);
        } else if ($categorized === 1) {
            return $query->whereNotNull(self::CATEGORY_ID);
        }

        return $query;
    }

    /**
     * Checks whether the course has category assigned.
     *
     * @return bool
     */
    public function hasCategory(): bool
    {
        return $this->category()->exists();
    }

    /**
     * Checks whether the course has any tags assigned.
     *
     * @return bool
     */
    public function hasTags(): bool
    {
        return $this->tags()->exists();
    }

    /**
     * Returns names of the tags associated with the course.
     *
     * @return SupportCollection
     */
    public function getTagsNames(): SupportCollection
    {
        return $this->tags()->pluck(Tag::NAME);
    }

    /**
     * Checks if course is past application deadline.
     *
     * @return bool
     */
    public function isPastApplicationDeadline(): bool
    {
        return Carbon::create($this->application_deadline)->lessThanOrEqualTo(now());
    }

    /**
     * Returns pretty-formatted "from" datetime.
     *
     * @return string|null
     */
    public function getFromDate(): ?string
    {
        return pretty_date($this->from);
    }

    /**
     * Returns pretty-formatted "to" datetime.
     *
     * @return string|null
     */
    public function getToDate(): ?string
    {
        return pretty_date($this->to);
    }

    /**
     * Returns pretty-formatted "application_deadline" datetime.
     *
     * @return string|null
     */
    public function getApplicationDeadlineDate(): ?string
    {
        return pretty_date($this->application_deadline);
    }
}
