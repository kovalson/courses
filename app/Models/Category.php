<?php
declare(strict_types=1);
namespace App\Models;

use App\Services\Filters\CategoryOrderFilter;
use App\Services\Filters\OrderDirectionFilter;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

/**
 * App\Models\Category
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $position
 * @property int|null $category_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Category|null $category
 * @property-read Collection|Course[] $courses
 * @property-read int|null $courses_count
 * @property-read Collection|Category[] $subcategories
 * @property-read int|null $subcategories_count
 * @method static Builder|Category newModelQuery()
 * @method static Builder|Category newQuery()
 * @method static Builder|Category ordered()
 * @method static Builder|Category query()
 * @method static Builder|Category whereCategoryId($value)
 * @method static Builder|Category whereCreatedAt($value)
 * @method static Builder|Category whereId($value)
 * @method static Builder|Category whereName($value)
 * @method static Builder|Category wherePosition($value)
 * @method static Builder|Category whereSlug($value)
 * @method static Builder|Category whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Category extends Model
{
    const ID            = "id";
    const NAME          = "name";
    const SLUG          = "slug";
    const POSITION      = "position";
    const CATEGORY_ID   = "category_id";
    const SUBCATEGORIES = "subcategories";

    use HasFactory;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var string[]|bool
     */
    protected $guarded = [
        self::ID,
    ];

    /**
     * Returns all courses that belong to the category.
     *
     * @return HasMany
     */
    public function courses(): HasMany
    {
        return $this->hasMany(Course::class);
    }

    /**
     * Returns the category it belongs to.
     *
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Returns all subcategories that belong to the category.
     *
     * @return HasMany
     */
    public function subcategories(): HasMany
    {
        return $this->hasMany(Category::class);
    }

    /**
     * Orders the query using request params.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        $column = get_filter_value(CategoryOrderFilter::class);
        $direction = get_filter_value(OrderDirectionFilter::class);

        return $query->orderBy($column, $direction);
    }

    /**
     * Checks whether the category has any subcategories.
     *
     * @return bool
     */
    public function hasSubcategories(): bool
    {
        return $this->subcategories()->exists();
    }

    /**
     * Checks whether the category has any siblings
     * in its branch.
     *
     * @return bool
     */
    public function hasSiblings(): bool
    {
        if ($this->category()->doesntExist()) {
            $count = Category::query()
                ->whereNull(self::CATEGORY_ID)
                ->count();
        } else {
            $count = $this->category
                ->subcategories()
                ->count();
        }

        return ($count > 1);
    }

    /**
     * Checks whether the category is first in order.
     *
     * @return bool
     */
    public function isFirst(): bool
    {
        return Category::query()
            ->where(self::CATEGORY_ID, $this->category_id)
            ->where(self::POSITION, "<", $this->position)
            ->doesntExist();
    }

    /**
     * Checks whether the category is last in order.
     *
     * @return bool
     */
    public function isLast(): bool
    {
        return Category::query()
            ->where(self::CATEGORY_ID, $this->category_id)
            ->where(self::POSITION, ">", $this->position)
            ->doesntExist();
    }

    /**
     * Generates new slug for the category name.
     *
     * @return void
     */
    public function generateSlug(): void
    {
        // We create the base slug
        $slug = Str::slug($this->name);

        // We check whether the base slug is already in use
        $slugInUse = Category::query()
            ->where(Category::SLUG, $slug)
            ->where(Category::ID, "!=", $this->id)
            ->exists();

        // If the slug is already in use we need to
        // suffix it with an order value
        if ($slugInUse) {
            // We check how many more slugs of the kind
            // are already in use
            $slugsOfTheKind = Category::query()
                ->where(Category::SLUG, "LIKE", $slug . "-%")
                ->where(Category::ID, "!=", $this->id)
                ->count();

            // We increment the count as this slug
            // is going to be the next one
            $slugsOfTheKind++;

            // We append the count to the slug
            $slug = $slug . "-" . $slugsOfTheKind;
        }

        // We set the course slug
        $this->slug = $slug;
    }
}
