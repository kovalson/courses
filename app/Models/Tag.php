<?php
declare(strict_types=1);
namespace App\Models;

use App\Services\Filters\OrderDirectionFilter;
use App\Services\Filters\TagOrderFilter;
use App\Traits\HasSlug;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection as SupportCollection;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * App\Models\Tag
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $type
 * @property int|null $order_column
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read array $translations
 * @method static Builder|Tag containing(string $name, $locale = null)
 * @method static Builder|Tag newModelQuery()
 * @method static Builder|Tag newQuery()
 * @method static Builder|Tag ordered()
 * @method static Builder|Tag query()
 * @method static Builder|Tag whereCreatedAt($value)
 * @method static Builder|Tag whereId($value)
 * @method static Builder|Tag whereName($value)
 * @method static Builder|Tag whereOrderColumn($value)
 * @method static Builder|Tag whereSlug($value)
 * @method static Builder|Tag whereType($value)
 * @method static Builder|Tag whereUpdatedAt($value)
 * @method static Builder|Tag withType(?string $type = null)
 * @mixin Eloquent
 * @property-read Collection|Course[] $courses
 * @property-read int|null $courses_count
 * @property int $published
 * @method static Builder|Tag published()
 * @method static Builder|Tag wherePublished($value)
 */
class Tag extends Model
{
    const NAME      = "name";
    const SLUG      = "slug";
    const PUBLISHED = "published";

    use HasFactory;
    use HasSlug;

    /**
     * Finds the tags by given names or creates new ones.
     *
     * @param array|string $values
     * @return SupportCollection|Tag[]|static
     */
    public static function findOrCreate($values)
    {
        $tags = collect($values)->map(function ($value) {
            if ($value instanceof self) {
                return $value;
            }

            return self::findOrCreateFromString($value);
        });

        return is_string($values) ? $tags->first() : $tags;
    }

    /**
     * Finds the tag by name string.
     *
     * @param string $name
     * @return Tag|null
     */
    protected static function findFromString(string $name): ?Tag
    {
        return self::query()
            ->where(self::NAME, $name)
            ->first();
    }

    /**
     * Finds or creates the tag from string.
     *
     * @param string $name
     * @return Tag
     */
    protected static function findOrCreateFromString(string $name): Tag
    {
        $tag = self::findFromString($name);

        if (!$tag) {
            $tag = self::create([
                self::NAME => $name,
            ]);
        }

        return $tag;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        self::NAME,
        self::SLUG,
        self::PUBLISHED,
    ];

    /**
     * Returns relationship with all courses that share this tag.
     *
     * @return BelongsToMany
     */
    public function courses(): BelongsToMany
    {
        return $this->belongsToMany(
            Course::class,
            "taggables",
            "tag_id",
            "taggable_id"
        );
    }

    /**
     * Scopes the query to published tags.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopePublished(Builder $query): Builder
    {
        if (auth()->check()) {
            return $query;
        }

        return $query->where(self::PUBLISHED, true);
    }

    /**
     * Orders the query using request parameters.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        $column = get_filter_value(TagOrderFilter::class);
        $direction = get_filter_value(OrderDirectionFilter::class);

        return $query->orderBy($column, $direction);
    }
}
