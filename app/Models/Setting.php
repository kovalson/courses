<?php
declare(strict_types=1);
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Setting
 *
 * @property int $id
 * @property string $type
 * @property string $key
 * @property string|null $value
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Setting newModelQuery()
 * @method static Builder|Setting newQuery()
 * @method static Builder|Setting query()
 * @method static Builder|Setting whereCreatedAt($value)
 * @method static Builder|Setting whereId($value)
 * @method static Builder|Setting whereKey($value)
 * @method static Builder|Setting whereType($value)
 * @method static Builder|Setting whereUpdatedAt($value)
 * @method static Builder|Setting whereValue($value)
 * @mixin Eloquent
 */
class Setting extends Model
{
    /**
     * Integer type setting.
     *
     * @type string
     */
    const INT = "int";

    /**
     * The limit of sidebar menu items.
     *
     * @type string
     */
    const MENU_ITEMS_LIMIT = "menu-items-limit";

    /**
     * The number of categories per page (for pagination).
     *
     * @type string
     */
    const CATEGORIES_PER_PAGE = "categories-per-page";

    /**
     * The number of courses per page (for pagination).
     *
     * @type string
     */
    const COURSES_PER_PAGE = "courses-per-page";

    /**
     * The number of tags per page (for pagination).
     *
     * @type string
     */
    const TAGS_PER_PAGE = "tags-per-page";

    use HasFactory;
}
