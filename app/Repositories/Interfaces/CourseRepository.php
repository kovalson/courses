<?php
declare(strict_types=1);
namespace App\Repositories\Interfaces;

use App\Http\Requests\Course\StoreRequest;
use App\Http\Requests\Course\UpdateRequest;
use App\Models\Course;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface CourseRepository extends RepositorySearch
{
    /**
     * Returns all courses.
     *
     * @return LengthAwarePaginator
     */
    public function all(): LengthAwarePaginator;

    /**
     * Finds the course by its slug.
     *
     * @param string $slug
     * @return Course|null
     */
    public function getBySlug(string $slug): ?Course;

    /**
     * Returns all courses that belong to given category.
     *
     * @param int $categoryId
     * @return LengthAwarePaginator
     */
    public function getByCategoryId(int $categoryId): LengthAwarePaginator;

    /**
     * Returns all courses with all of given tags.
     *
     * @param array $tags
     * @return LengthAwarePaginator
     */
    public function getByAllTags(array $tags): LengthAwarePaginator;

    /**
     * Returns all courses with any of given tags.
     *
     * @param array $tags
     * @return LengthAwarePaginator
     */
    public function getByAnyTags(array $tags): LengthAwarePaginator;

    /**
     * Creates new course from request.
     *
     * @param StoreRequest $request
     * @return bool
     */
    public function createFromRequest(StoreRequest $request): bool;

    /**
     * Updates the course using request attributes.
     *
     * @param string $slug
     * @param UpdateRequest $request
     * @return bool
     */
    public function updateFromRequest(string $slug, UpdateRequest $request): bool;

    /**
     * Updates the course.
     *
     * @param string $slug
     * @param array $attributes
     * @return bool
     */
    public function update(string $slug, array $attributes): bool;

    /**
     * Deletes course by slug.
     *
     * @param string $slug
     * @return bool
     */
    public function deleteOne(string $slug): bool;
}
