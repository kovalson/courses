<?php
declare(strict_types=1);
namespace App\Repositories\Interfaces;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface RepositorySearch
{
    /**
     * Searches courses using given query.
     *
     * @param string $queryText
     * @return LengthAwarePaginator
     */
    public function search(string $queryText): LengthAwarePaginator;
}
