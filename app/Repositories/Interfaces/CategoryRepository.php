<?php
declare(strict_types=1);
namespace App\Repositories\Interfaces;

use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Models\Category;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface CategoryRepository extends RepositorySearch
{
    /**
     * Returns structured categories.
     *
     * @return Collection
     */
    public function structured(): Collection;

    /**
     * Creates new category from request object.
     *
     * @param StoreRequest $request
     * @return Category
     */
    public function createFromRequest(StoreRequest $request): Category;

    /**
     * Updates the category using attributes from request object.
     *
     * @param string $slug
     * @param UpdateRequest $request
     * @return Category
     */
    public function updateFromRequest(string $slug, UpdateRequest $request): Category;

    /**
     * Moves the category up in order.
     *
     * @param string $slug
     */
    public function moveUp(string $slug): void;

    /**
     * Moves the category down in order.
     *
     * @param string $slug
     */
    public function moveDown(string $slug): void;

    /**
     * Returns categories that have no parent category.
     *
     * @return Collection
     */
    public function getBaseCategories(): Collection;

    /**
     * Returns the category by its slug.
     *
     * @param string $slug
     * @return Category|null
     */
    public function getBySlug(string $slug): ?Category;

    /**
     * Returns categories that belong to category of given slug.
     *
     * @param int $categoryId
     * @return LengthAwarePaginator
     */
    public function getCategoriesByCategoryId(int $categoryId): LengthAwarePaginator;

    /**
     * Returns all categories grouped in an array.
     *
     * @return array
     */
    public function getGrouped(): array;

    /**
     * Deletes category by slug.
     *
     * @param string $slug
     * @return bool
     */
    public function deleteOne(string $slug): bool;
}
