<?php
declare(strict_types=1);
namespace App\Repositories\Interfaces;

use App\Models\Tag;
use Illuminate\Support\Collection;

interface TagRepository extends RepositorySearch
{
    /**
     * Returns all tags wrapped in collection.
     *
     * @return Collection
     */
    public function all(): Collection;

    /**
     * Returns the tag by its slug.
     *
     * @param string $slug
     * @return Tag|null
     */
    public function getBySlug(string $slug): ?Tag;
}
