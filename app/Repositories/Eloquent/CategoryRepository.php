<?php
declare(strict_types=1);
namespace App\Repositories\Eloquent;

use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Models\Category;
use App\Models\Setting;
use App\Repositories\Interfaces\CategoryRepository as CategoryRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Throwable;

class CategoryRepository implements CategoryRepositoryInterface
{
    /**
     * Returns structured categories.
     *
     * @return Collection
     */
    public function structured(): Collection
    {
        return Category::query()
            ->with(Category::SUBCATEGORIES, function ($query) {
                $query->orderBy(Category::POSITION);
            })
            ->whereNull(Category::CATEGORY_ID)
            ->orderBy(Category::POSITION)
            ->get();
    }

    /**
     * Creates new category from request object.
     *
     * @param StoreRequest $request
     * @return Category
     */
    public function createFromRequest(StoreRequest $request): Category
    {
        // We get only required attributes
        $attributes = $request->only([
            Category::NAME,
            Category::CATEGORY_ID,
        ]);

        // We count the categories with the same
        // parent category
        $categoriesOfTheKind = Category::query()
            ->where(Category::CATEGORY_ID, $attributes[Category::CATEGORY_ID])
            ->count();

        // We append other required attributes
        $attributes[Category::POSITION] = $categoriesOfTheKind + 1;

        // We create the category
        return Category::create($attributes);
    }

    /**
     * Updates the category using attributes from request object.
     *
     * @param string $slug
     * @param UpdateRequest $request
     * @return Category
     */
    public function updateFromRequest(string $slug, UpdateRequest $request): Category
    {
        // We get only required attributes
        $attributes = $request->only([
            Category::NAME,
            Category::CATEGORY_ID,
        ]);

        // We get the category or throw an exception
        $category = Category::query()
            ->where(Category::SLUG, $slug)
            ->firstOrFail();

        // We update the category
        $category->update($attributes);

        return $category;
    }

    /**
     * Moves the category up in order.
     *
     * @param string $slug
     */
    public function moveUp(string $slug): void
    {
        // We get the category or throw an exception
        $category = Category::query()
            ->where(Category::SLUG, $slug)
            ->firstOrFail();

        // We cannot move up the first category in order
        if ($category->isFirst()) {
            return;
        }

        // We get the category that is above the target category.
        // There must exist at least one such category, as we
        // checked before that this is not the first one
        $categoryAbove = Category::query()
            ->where(Category::CATEGORY_ID, $category->category_id)
            ->where(Category::POSITION, $category->position - 1)
            ->firstOrFail();

        // We swap the categories orders
        $this->swapCategoriesOrders($category, $categoryAbove);
    }

    /**
     * Moves the category down in order.
     *
     * @param string $slug
     */
    public function moveDown(string $slug): void
    {
        // We get the category or throw an exception
        $category = Category::query()
            ->where(Category::SLUG, $slug)
            ->firstOrFail();

        // We cannot move down the last category in order
        if ($category->isLast()) {
            return;
        }

        // We get the category that is below the target category.
        // There must exist at least one such category, as we
        // checked before that this is not the last one
        $categoryAbove = Category::query()
            ->where(Category::CATEGORY_ID, $category->category_id)
            ->where(Category::POSITION, $category->position + 1)
            ->firstOrFail();

        // We swap the categories orders
        $this->swapCategoriesOrders($category, $categoryAbove);
    }

    /**
     * Returns categories that have no parent category.
     *
     * @return Collection
     */
    public function getBaseCategories(): Collection
    {
        return Category::query()
            ->whereNull(Category::CATEGORY_ID)
            ->get();
    }

    /**
     * Returns the category by its slug.
     *
     * @param string $slug
     * @return Category|null
     */
    public function getBySlug(string $slug): ?Category
    {
        return Category::query()
            ->where(Category::SLUG, $slug)
            ->first();
    }

    /**
     * Returns categories that belong to category of given slug.
     *
     * @param int $categoryId
     * @return LengthAwarePaginator
     */
    public function getCategoriesByCategoryId(int $categoryId): LengthAwarePaginator
    {
        $perPage = setting(Setting::CATEGORIES_PER_PAGE);

        return Category::query()
            ->where(Category::CATEGORY_ID, $categoryId)
            ->orderBy(Category::POSITION)
            ->paginate($perPage);
    }

    /**
     * Returns all categories grouped in an array.
     *
     * @return array
     */
    public function getGrouped(): array
    {
        $grouped = [];

        $categories = Category::query()
            ->whereNull(Category::CATEGORY_ID)
            ->get();

        foreach ($categories as $category) {
            if ($category->hasSubcategories()) {
                $grouped[$category->name] = $category->subcategories
                    ->pluck(Category::NAME, Category::ID)
                    ->toArray();
            }
        }

        return $grouped;
    }

    /**
     * Searches categories using given query.
     *
     * @param string $queryText
     * @return LengthAwarePaginator
     */
    public function search(string $queryText): LengthAwarePaginator
    {
        $perPage = setting(Setting::CATEGORIES_PER_PAGE);
        $fullQueryText = "%" . $queryText . "%";

        return Category::query()
            ->ordered()
            ->where(Category::NAME, "LIKE", $fullQueryText)
            ->orWhereHas("category", function (Builder $query) use ($fullQueryText) {
                $query->where(Category::NAME, "LIKE", $fullQueryText);
            })
            ->paginate($perPage);
    }

    /**
     * Deletes category by slug.
     *
     * @param string $slug
     * @return bool
     */
    public function deleteOne(string $slug): bool
    {
        $category = Category::query()
            ->where(Category::SLUG, $slug)
            ->firstOrFail();

        try {
            $category->delete();
        } catch (Throwable $exception) {
            return false;
        }

        return true;
    }

    /**
     * Swaps categories orders.
     *
     * @param Category $source
     * @param Category $target
     * @return void
     */
    private function swapCategoriesOrders(Category $source, Category $target): void
    {
        // We save the source position for later
        $sourcePosition = $source->position;

        // We update the source category
        $source->update([
            Category::POSITION => $target->position,
        ]);

        // We update the target category
        $target->update([
            Category::POSITION => $sourcePosition,
        ]);
    }
}
