<?php
declare(strict_types=1);
namespace App\Repositories\Eloquent;

use App\Models\Setting;
use App\Models\Tag;
use App\Repositories\Interfaces\TagRepository as TagRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class TagRepository implements TagRepositoryInterface
{
    /**
     * Returns all tags wrapped in collection.
     *
     * @return Collection
     */
    public function all(): Collection
    {
        return Tag::all();
    }

    /**
     * Returns the tag by its slug.
     *
     * @param string $slug
     * @return Tag|null
     */
    public function getBySlug(string $slug): ?Tag
    {
        return Tag::query()
            ->where("slug", $slug)
            ->first();
    }

    /**
     * Searches courses using given query.
     *
     * @param string $queryText
     * @return LengthAwarePaginator
     */
    public function search(string $queryText): LengthAwarePaginator
    {
        $fullQueryText = "%" . $queryText . "%";
        $perPage = setting(Setting::TAGS_PER_PAGE);

        return Tag::query()
            ->published()
            ->ordered()
            ->where("name", "LIKE", $fullQueryText)
            ->paginate($perPage);
    }
}
