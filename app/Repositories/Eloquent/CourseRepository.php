<?php
declare(strict_types=1);
namespace App\Repositories\Eloquent;

use App\Http\Requests\Course\BaseRequest;
use App\Http\Requests\Course\StoreRequest;
use App\Http\Requests\Course\UpdateRequest;
use App\Models\Category;
use App\Models\Course;
use App\Models\Setting;
use App\Models\Tag;
use App\Repositories\Interfaces\CourseRepository as CourseRepositoryInterface;
use App\Services\Filters\CourseArchivingFilter;
use App\Services\Filters\CourseCategoryFilter;
use App\Services\Filters\CoursePublicationFilter;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Throwable;

class CourseRepository implements CourseRepositoryInterface
{
    /**
     * Returns all courses.
     *
     * @return LengthAwarePaginator
     */
    public function all(): LengthAwarePaginator
    {
        $perPage = setting(Setting::COURSES_PER_PAGE);
        $published = get_filter_value(CoursePublicationFilter::class);
        $archived = get_filter_value(CourseArchivingFilter::class);
        $categorized = get_filter_value(CourseCategoryFilter::class);

        return Course::query()
            ->published($published)
            ->active($archived)
            ->categorized($categorized)
            ->ordered()
            ->paginate($perPage);
    }

    /**
     * Finds the course by its slug.
     *
     * @param string $slug
     * @return Course|null
     */
    public function getBySlug(string $slug): ?Course
    {
        $query = Course::query()->where(Course::SLUG, $slug);

        if (auth()->guest()) {
            $query->where(Course::PUBLISHED, true);
        }

        return $query->first();
    }

    /**
     * Returns all courses that belong to given category.
     *
     * @param int $categoryId
     * @param bool|null $published
     * @param bool|null $archived
     * @return LengthAwarePaginator
     */
    public function getByCategoryId(int $categoryId, ?bool $published = true, ?bool $archived = null): LengthAwarePaginator
    {
        $perPage = setting(Setting::COURSES_PER_PAGE);

        return Course::query()
            ->published()
            ->active()
            ->ordered()
            ->where(Course::CATEGORY_ID, $categoryId)
            ->paginate($perPage);
    }

    /**
     * Returns all courses with all of given tags.
     *
     * @param array $tags
     * @param bool|null $published
     * @param bool|null $archived
     * @return LengthAwarePaginator
     */
    public function getByAllTags(array $tags, ?bool $published = true, ?bool $archived = null): LengthAwarePaginator
    {
        $perPage = setting(Setting::COURSES_PER_PAGE);

        return Course::query()
            ->withAllTags($tags)
            ->published()
            ->active()
            ->ordered()
            ->paginate($perPage);
    }

    /**
     * Returns all courses with any of given tags.
     *
     * @param array $tags
     * @param bool|null $published
     * @param bool|null $archived
     * @return LengthAwarePaginator
     */
    public function getByAnyTags(array $tags, ?bool $published = true, ?bool $archived = null): LengthAwarePaginator
    {
        $perPage = setting(Setting::COURSES_PER_PAGE);

        return Course::query()
            ->withAnyTags($tags)
            ->published()
            ->active()
            ->ordered()
            ->paginate($perPage);
    }

    /**
     * Searches categories using given query.
     *
     * @param string $queryText
     * @param bool|null $published
     * @param bool|null $archived
     * @return LengthAwarePaginator
     */
    public function search(string $queryText, ?bool $published = true, ?bool $archived = null): LengthAwarePaginator
    {
        $perPage = setting(Setting::COURSES_PER_PAGE);
        $fullQueryText = "%" . $queryText . "%";
        $published = get_filter_value(CoursePublicationFilter::class);
        $archived = get_filter_value(CourseArchivingFilter::class);

        return Course::query()
            ->published($published)
            ->active($archived)
            ->ordered()
            ->where(function (Builder $query) use ($fullQueryText) {
                $query->where(Course::TITLE, "LIKE", $fullQueryText)
                    ->orWhere(Course::FROM, "LIKE", $fullQueryText)
                    ->orWhere(Course::TO, "LIKE", $fullQueryText)
                    ->orWhere(Course::APPLICATION_DEADLINE, "LIKE", $fullQueryText)
                    ->orWhere(Course::PLACE, "LIKE", $fullQueryText)
                    ->orWhereHas("category", function (Builder $query) use ($fullQueryText) {
                        $query->where(Category::NAME, "LIKE", $fullQueryText);
                    });
            })
            ->paginate($perPage);
    }

    /**
     * Creates new course from request.
     *
     * @param StoreRequest $request
     * @return bool
     */
    public function createFromRequest(StoreRequest $request): bool
    {
        // We get the attributes to store
        $attributes = $this->getAttributesFromRequest($request);

        // We create the course
        $course = Course::create($attributes);

        // We attach the tags
        $course->attachTags($request->tags);

        return true;
    }

    /**
     * Updates the course using request attributes.
     *
     * @param string $slug
     * @param UpdateRequest $request
     * @return bool
     */
    public function updateFromRequest(string $slug, UpdateRequest $request): bool
    {
        // We get the attributes to update
        $attributes = $this->getAttributesFromRequest($request);

        // We get the course or throw an exception
        $course = Course::query()
            ->where(Course::SLUG, $slug)
            ->firstOrFail();

        // We update the course
        $course->update($attributes);

        // We synchronize the tags
        $course->syncTags($request->tags);

        return true;
    }

    /**
     * Updates the course.
     *
     * @param string $slug
     * @param array $attributes
     * @return bool
     */
    public function update(string $slug, array $attributes): bool
    {
        // We find the course or fail
        $course = Course::query()
            ->where(Course::SLUG, $slug)
            ->firstOrFail();

        // We publish all tags assigned to the course
        // if the course was published as well
        if (isset($attributes[Course::PUBLISHED]) && $attributes[Course::PUBLISHED]) {
            $course->tags()->update([
                Tag::PUBLISHED => true,
            ]);
        }

        // We update the course
        return $course->update($attributes);
    }

    /**
     * Deletes course by slug.
     *
     * @param string $slug
     * @return bool
     */
    public function deleteOne(string $slug): bool
    {
        $course = Course::query()
            ->where(Course::SLUG, $slug)
            ->firstOrFail();

        try {
            $course->delete();
        } catch (Throwable $exception) {
            return false;
        }

        return true;
    }

    /**
     * Returns attributes for storing or updating the course.
     *
     * @param BaseRequest $request
     * @return array
     */
    private function getAttributesFromRequest(BaseRequest $request): array
    {
        // We only get mass-assignable attributes
        $attributes = $request->only([
            Course::TITLE,
            Course::CATEGORY_ID,
            Course::IMAGE_URL,
            Course::FROM,
            Course::TO,
            Course::APPLICATION_DEADLINE,
            Course::PLACE,
            Course::DESCRIPTION,
        ]);

        // We modify some attributes before inserting into database
        $attributes[Course::FROM] = Carbon::createFromFormat("d.m.Y H:i", $attributes[Course::FROM])->toDateTimeString();
        $attributes[Course::TO] = Carbon::createFromFormat("d.m.Y H:i", $attributes[Course::TO])->toDateTimeString();
        $attributes[Course::APPLICATION_DEADLINE] = Carbon::createFromFormat("d.m.Y H:i", $attributes[Course::APPLICATION_DEADLINE])->toDateTimeString();

        // We append attributes that are available
        // only for authorized users
        if (auth()->check()) {
            $attributes[Course::PUBLISHED] = $request->has(Course::PUBLISHED);
            $attributes[Course::ARCHIVED] = $request->has(Course::ARCHIVED);
        }

        return $attributes;
    }
}
