<?php
declare(strict_types=1);
namespace App\Providers;

use Form;
use Illuminate\Support\ServiceProvider;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Form::component("bsSubmit", "components.form.button", [
            "value",
            "params" => [],
        ]);

        Form::component("bsText", "components.form.text", [
            "name",
            "value" => null,
            "params" => [],
            "defaultParams" => [],
        ]);

        Form::component("bsEmail", "components.form.text", [
            "name" => "email",
            "value" => null,
            "params" => [],
            "defaultParams" => [
                "type" => "email",
                "label" => __("Adres e-mail"),
            ],
        ]);

        Form::component("bsPassword", "components.form.text", [
            "name" => "password",
            "value" => null,
            "params" => [],
            "defaultParams" => [
                "type" => "password",
                "label" => __("Hasło"),
            ],
        ]);

        Form::component("bsSelect", "components.form.select", [
            "name",
            "options" => [],
            "selected" => null,
            "params" => [],
        ]);

        Form::component("bsCheckbox", "components.form.checkbox", [
            "name",
            "value",
            "params" => [
                "label"
            ],
        ]);

        Form::component("bsCaptcha", "components.form.captcha", [
            "name" => "captcha",
            "params" => [],
        ]);

        Form::component("bsTags", "components.form.tags", [
            "name",
            "tags" => [],
            "selected" => [],
            "params" => [],
        ]);
    }
}
