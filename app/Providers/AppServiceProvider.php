<?php
declare(strict_types=1);
namespace App\Providers;

use App\Models\Category;
use App\Models\Course;
use App\Models\Setting;
use App\Models\Tag;
use App\Observers\CategoryObserver;
use App\Observers\CourseObserver;
use App\Observers\TagObserver;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setCustomPaginationView();
        $this->writeSettingsToConfig();
        $this->registerObservers();

        // We set the default string length for DB
        Schema::defaultStringLength(191);
    }

    /**
     * Sets new custom pagination view.
     *
     * @return void
     */
    private function setCustomPaginationView(): void
    {
        Paginator::defaultView("partials.pagination");
    }

    /**
     * Saves app settings from database to config array.
     *
     * @return void
     */
    private function writeSettingsToConfig(): void
    {
        if (Schema::hasTable("settings")) {
            $settings = Setting::all();

            foreach ($settings as $setting) {
                $key = "settings." . $setting->key;
                $value = $setting->value;

                Config::set($key, $value);
            }
        }
    }

    /**
     * Registers models observers.
     *
     * @return void
     */
    private function registerObservers(): void
    {
        Tag::observe(TagObserver::class);
        Course::observe(CourseObserver::class);
        Category::observe(CategoryObserver::class);
    }
}
