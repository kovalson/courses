<?php
declare(strict_types=1);

use App\Http\Controllers\Admin\CategoryController as AdminCategoryController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\TagController;
use Illuminate\Support\Facades\Route;

Route::auth([
    "register" => false,
    "reset" => false,
    "verify" => false,
]);

Route::group(["middleware" => "auth"], function () {
    Route::get("/kategorie", [AdminCategoryController::class, "index"])->name("categories.index");
    Route::get("/kategorie/dodaj", [AdminCategoryController::class, "create"])->name("categories.create");
    Route::get("/kategorie/{slug}/edytuj", [AdminCategoryController::class, "edit"])->name("categories.edit");
    Route::get("/kategorie/{slug}/przesun/gora", [AdminCategoryController::class, "moveUp"])->name("categories.move.up");
    Route::get("/kategorie/{slug}/przesun/dol", [AdminCategoryController::class, "moveDown"])->name("categories.move.down");
    Route::get("/kategorie/{slug}/usun", [AdminCategoryController::class, "destroy"])->name("categories.destroy");
    Route::post("/kategorie", [AdminCategoryController::class, "store"])->name("categories.store");
    Route::post("/kategorie/{slug}", [AdminCategoryController::class, "update"])->name("categories.update");

    Route::get("/szkolenie/{course}/edytuj", [CourseController::class, "edit"])->name("course.edit");
    Route::get("/szkolenie/{course}/usun", [CourseController::class, "destroy"])->name("course.destroy");
    Route::get("/szkolenie/{course}/ukryj", [CourseController::class, "hide"])->name("course.hide");
    Route::get("/szkolenie/{course}/opublikuj", [CourseController::class, "publish"])->name("course.publish");
    Route::get("/szkolenie/{course}/archiwizuj", [CourseController::class, "archive"])->name("course.archive");
    Route::get("/szkolenie/{course}/przywroc", [CourseController::class, "restore"])->name("course.restore");
    Route::post("/szkolenie/{course}", [CourseController::class, "update"])->name("course.update");
});

Route::get("/kategoria/{slug}/szkolenia", [CategoryController::class, "courses"])->name("category.courses");
Route::get("/kategoria/{slug}", [CategoryController::class, "categories"])->name("category.categories");
Route::get("/szkolenie/{course}", [CourseController::class, "show"])->name("course");
Route::get("/dodaj", [CourseController::class, "create"])->name("course.create");
Route::post("/dodaj", [CourseController::class, "store"])->name("course.store");
Route::get("/tag/{tag}", TagController::class)->name("tag");
Route::get("/wyszukiwanie", SearchController::class)->name("search");
Route::get("/", [HomeController::class, "index"])->name("home");
